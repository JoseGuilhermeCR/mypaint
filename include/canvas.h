/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef CANVAS_H_
#define CANVAS_H_

#include "color.h"
#include "rect.h"

#include <stdint.h>

#include <SDL2/SDL.h>

struct toolbox;

struct line {
    int32_t start_x;
    int32_t start_y;
    int32_t end_x;
    int32_t end_y;
    uint8_t type;
};

struct circumference {
    int32_t center_x;
    int32_t center_y;
    int32_t radius;
};

/**
 * @brief Inicializa o canvas.
 *
 * @param placement Retângulo que indica tamanho / posição do botão.
 * @param parent_bounds Retângulo que indica tamanho / posição do "pai" do
 * botão.
 *
 * @return uint8_t
 */
extern uint8_t canvas_init(const struct rect *placement,
                           const struct rect *parent_bounds);

/**
 * @brief Destrói canvas e desaloca a memória usada por ele.
 *
 *
 * @return uint8_t
 */
extern uint8_t canvas_destroy();

/**
 * @brief Renderiza o canvas.
 *
 *
 * @return uint8_t
 */
extern uint8_t canvas_render();

/**
 * @brief Cuida de input do mouse.
 *
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 *
 * @return uint8_t
 */
extern uint8_t canvas_handle_mouse(uint32_t state, int32_t x, int32_t y);

/**
 * @brief Configura a cor de fundo do canvas.
 *
 * @param c Ponteiro para estrutura da cor.
 *
 * @return uint8_t
 */
extern uint8_t canvas_set_bg_color(const struct color *c);

/**
 * @brief Configura a cor de frente do canvas.
 *
 * @param c Ponteiro para estrutura da cor.
 *
 * @return uint8_t
 */
extern uint8_t canvas_set_fg_color(const struct color *c);

/**
 * @brief Liga opções em um pixel.
 *
 * @param x Coordenada x do pixel no canvas.
 * @param y Coordenada y do pixel no canvas.
 *
 * @return uint8_t
 */
extern uint8_t canvas_set_pixel_coord(int32_t x, int32_t y);

/**
 * @brief Retorna o retângulo que determina a posição e o tamanho do canvas.
 *
 * @return const struct rect* ou NULO se o canvas estiver desinicializado.
 */
extern const struct rect *canvas_get_placement();

/**
 * @brief Função que desabilita qualquer máquina de estado.
 *
 */
extern void canvas_quit_state();

// -----------------------------------------------Abaixo, somente callbacks de
// botões-----------------------------------------------

/**
 * @brief Limpa todo o canvas.
 *
 */
extern void canvas_clear();

/**
 * @brief Função que habilita a máquina de estado para rasterização dda.
 *
 */
extern void canvas_set_draw_dda_line_state();

/**
 * @brief Função que habilita a máquina de estado para rasterização bresenham.
 *
 */
extern void canvas_set_draw_bresenham_line_state();

/**
 * @brief Função que habilita máquina de estado para modo de desenho.
 *
 */
extern void canvas_set_drawing_state();

/**
 * @brief Função que habilita máquina de estado para rasterização de uma
 * circunferência.
 *
 */
extern void canvas_set_draw_bresenham_circumference_state();

/**
 * @brief Translada canvas em X positivo.
 *
 */
extern void canvas_translate_positive_x();

/**
 * @brief Translada canvas em X negativo.
 *
 */
extern void canvas_translate_negative_x();

/**
 * @brief Translada canvas em Y positivo.
 *
 */
extern void canvas_translate_positive_y();

/**
 * @brief Translada canvas em Y negativo.
 *
 */
extern void canvas_translate_negative_y();

/**
 * @brief Rotaciona canvas.
 *
 */
extern void canvas_rotate();

/**
 * @brief Escala canvas.
 *
 */
extern void canvas_scale();

/**
 * @brief Reflete X em canvas.
 *
 */
extern void canvas_reflect_x();

/**
 * @brief Reflete Y em canvas.
 *
 */
extern void canvas_reflect_y();

/**
 * @brief Reflete X/Y em canvas.
 *
 */
extern void canvas_reflect_xy();

/**
 * @brief Função que habilita máquina de estado para line clipping com cohen
 * sutherland.
 *
 */
extern void canvas_set_cohen_sutherland_state();

/**
 * @brief Função que habilita máquina de estado para line clipping com liang
 * barsky.
 *
 */
extern void canvas_set_liang_barsky_state();

/**
 * @brief Função que habilita e desabilita os eixos no canvas.
 *
 */
extern void canvas_toggle_axis();

#endif
