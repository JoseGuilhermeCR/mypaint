/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef FONT_H_
#define FONT_H_

#include "color.h"
#include "window.h"

#include <stdint.h>

#include <SDL2/SDL.h>

#define NOTHING_SELECTED_TEXT_ID       ((int32_t)0x1000)
#define POSITIVE_NUMERAL_TEXTURE_COUNT ((int32_t)361)
#define NEGATIVE_NUMERAL_TEXTURE_COUNT ((int32_t)0)

enum FONT_SIZE
{
    FONT_SIZE_SMALL = 0,
    FONT_SIZE_MEDIUM,
    FONT_SIZE_BIG,

    FONT_SIZE_ENUM_SIZE
};

struct text {
    SDL_Texture *texture;
    uint32_t w;
    uint32_t h;
};

/**
 * @brief Inicializa o sistema de fonte.
 *
 * @return 0
 * @return 1
 */
extern uint8_t font_init();

/**
 * @brief Destrói o sistema de fonte.
 *
 * @return 0
 * @return 1
 */
extern uint8_t font_destroy();

/**
 * @brief Cria uma textura para um texto. Essa textura é guardada internamente
 * de forma que ninguém precise desalocar memória.
 *
 * @param text Texto que será criado.
 * @param size Tamanho do texto.
 * @param color Cor do texto.
 * @param forced_id ID específico (caso não se deseje um gerado
 * automaticamente).
 * @return int32_t ID do texto gerado.
 */
extern int32_t font_create_texture(const char *text,
                                   enum FONT_SIZE size,
                                   struct color color,
                                   int32_t forced_id);

/**
 * @brief Retorna texto que tem o conteúdo igual com parâmetro.
 *
 * @param str Texto que será comparado.
 * @return struct text* ou NULO caso nenhum texto seja encontrado.
 */
extern struct text *font_get_text_with_str(const char *str);

/**
 * @brief Retorna texto que tem o id igual a parâmetro.
 *
 * @param id Id que será comparado.
 * @return struct text* ou NULO caso nenhum texto seja encontrado.
 */
extern struct text *font_get_text_with_id(int32_t id);

#endif
