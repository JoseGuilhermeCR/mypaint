/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef BUTTON_H_
#define BUTTON_H_

#include "font.h"
#include "rect.h"

#include <stdint.h>

#include <SDL2/SDL.h>

#define HAS_BORDER      ((uint8_t)0x01)
#define HAS_HOVER_COLOR ((uint8_t)0x02)
#define IS_MODE_BUTTON  ((uint8_t)0x04)

struct canvas;
struct toolbox;

struct button {
    void (*callback)();

    struct rect placement;
    struct rect text_placement;
    const struct text *text;

    uint32_t debounce_timer;

    uint8_t is_initialized;
    uint8_t is_hovered;
    uint8_t options;
};

/**
 * @brief Inicializa a estrutura do botão.
 *
 * @param btn Ponteiro para estrutura.
 * @param placement Retângulo que indica tamanho / posição do botão.
 * @param parent_bounds Retângulo que indica tamanho / posição do "pai" do
 * botão.
 * @param options Opções extras que o botão pode receber.
 *
 * @return uint8_t
 */
extern uint8_t button_init(struct button *btn,
                           const struct rect *placement,
                           const struct rect *parent_bounds,
                           uint8_t options);

/**
 * @brief Destrói a estrutura do botão.
 *
 * @param btn Ponteiro para estrutura.
 *
 * @return uint8_t
 */
extern uint8_t button_destroy(struct button *btn);

/**
 * @brief Renderiza a estrutura do botão.
 *
 * @param btn Ponteiro para estrutura.
 *
 * @return uint8_t
 */
extern uint8_t button_render(struct button *btn);

/**
 * @brief Cuida de input do mouse.
 *
 * @param btn Ponteiro para estrutura.
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 *
 * @return uint8_t
 */
extern uint8_t button_handle_mouse(struct button *btn,
                                   uint32_t state,
                                   int32_t x,
                                   int32_t y);

/**
 * @brief Configura o callback que será chamado quando esse botão for clicado.
 *
 * @param btn Ponteiro para estrutura.
 * @param callback Ponteiro para função.
 *
 * @return uint8_t
 */
extern uint8_t button_set_callback(struct button *btn, void (*callback)());

/**
 * @brief Configura o texto que será mostrado no botão.
 *
 * @param btn Ponteiro para estrutura.
 * @param text Ponteiro para texto.
 *
 * @return uint8_t
 */
extern uint8_t button_set_text(struct button *btn, const struct text *text);

#endif
