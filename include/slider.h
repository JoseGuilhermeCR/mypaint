/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SLIDER_H_
#define SLIDER_H_

#include "color.h"
#include "rect.h"

enum SLIDER_STATES
{
    SLIDER_STATE_NOT_SELECTED = 0,
    SLIDER_STATE_SELECTED,
    SLIDER_STATE_DRAG,
};

struct slider {
    struct rect placement;
    struct rect cursor_placement;
    struct rect bar_placement;

    struct color bar_color;

    int32_t min;
    int32_t max;
    int32_t value;

    enum SLIDER_STATES state;

    uint8_t is_initialized;
};

/**
 *
 * @brief Inicializa a estrutura do slider.
 *
 * @param s Ponteiro para estrutura.
 * @param placement Retângulo que indica tamanho / posição do botão.
 * @param parent_bounds Retângulo que indica tamanho / posição do "pai" do
 * botão.
 * @return 0
 * @return 1
 */
extern uint8_t slider_init(struct slider *s,
                           const struct rect *placement,
                           const struct rect *parent_bounds);

/**
 * @brief Destrói estrutura do slider.
 *
 * @param s Ponteiro para estrutura.
 * @return 0
 * @return 1
 */
extern uint8_t slider_destroy(struct slider *s);

/**
 * @brief Renderiza slider.
 *
 * @param s Ponteiro para estrutura.
 * @return 0
 * @return 1
 */
extern uint8_t slider_render(struct slider *s);

/**
 * @brief Cuida do input do mouse.
 *
 * @param s Ponteiro para estrutura.
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 * @return 0
 * @return 1
 */
extern uint8_t slider_handle_mouse(struct slider *s,
                                   uint32_t state,
                                   int32_t x,
                                   int32_t y);

/**
 * @brief Configura valor mínimo e máximo do slider.
 *
 * @param s Ponteiro para estrutura.
 * @param min valor mínimo.
 * @param max valor máximo.
 * @return 0
 * @return 1
 */
extern uint8_t slider_set_min_max(struct slider *s, int32_t min, int32_t max);

/**
 * @brief Configura valor do slider, deve estar entre min e max.
 *
 * @param s Ponteiro para estrutura.
 * @param value Valor.
 * @return 0
 * @return 1
 */
extern uint8_t slider_set_value(struct slider *s, int32_t value);

/**
 * @brief Configura a cor da barra do slider.
 *
 * @param s Ponteiro para estrutura.
 * @param color Ponteiro para estrutura da cor.
 * @return 0
 * @return 1
 */
extern uint8_t slider_set_bar_color(struct slider *s,
                                    const struct color *color);

#endif
