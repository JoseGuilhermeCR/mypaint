/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef TOOLBOX_H_
#define TOOLBOX_H_

#include "rect.h"

#include <stdint.h>

#include <SDL2/SDL.h>

struct text;
struct button;
struct canvas;

struct button_entry {
    struct button *button;
    struct button_entry *next;
};

struct toolbox {
    struct button_entry *button_list_root;
    const struct text *current_mode_text;

    struct rect placement;
    struct rect current_mode_text_placement;
};

/**
 * @brief Inicializa toolbox.
 *
 * @param placement Retângulo que indica tamanho / posição do botão.
 * @param parent_bounds Retângulo que indica tamanho / posição do "pai" do
 * botão.
 * @return 0
 * @return 1
 */
extern uint8_t toolbox_init(const struct rect *placement,
                            const struct rect *parent_bounds);

/**
 * @brief Destrói toolbox.
 *
 * @return 0
 * @return 1
 */
extern uint8_t toolbox_destroy();

/**
 * @brief Renderiza toolbox.
 *
 * @return 0
 * @return 1
 */
extern uint8_t toolbox_render();

/**
 * @brief Cuida de input do mouse.
 *
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 * @return 0
 * @return 1
 */
extern uint8_t toolbox_handle_mouse(uint32_t state, int32_t x, int32_t y);

/**
 * @brief Adiciona um botão ao toolbox.
 *
 * @param button Ponteiro para estrutura do botão que será adicionado.
 * @return 0
 * @return 1
 */
extern uint8_t toolbox_attach_button(struct button *button);

/**
 * @brief Atualiza o texto da ferramenta atual.
 *
 * @param text Ponteiro para texto que será usado.
 * @return 0
 * @return 1
 */
extern uint8_t toolbox_set_current_mode_text(const struct text *text);

/**
 * @brief Retorna posição / tamanho da toolbox.
 *
 * @return const struct rect*
 */
extern const struct rect *toolbox_get_placement();

#endif
