/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef WINDOW_H_
#define WINDOW_H_

#include "rect.h"

#include <stdint.h>

#include <SDL2/SDL.h>

#define WINDOW_IDEAL_MONITOR_WIDTH  ((int32_t)1920)
#define WINDOW_IDEAL_MONITOR_HEIGHT ((int32_t)1080)

#define WINDOW_DESIRED_WIDTH  ((int32_t)1600)
#define WINDOW_DESIRED_HEIGHT ((int32_t)900)

/**
 * @brief Inicializa janela.
 *
 * @return 0
 * @return 1
 */
extern uint8_t window_init();

/**
 * @brief Destrói a janela.
 *
 * @return 0
 * @return 1
 */
extern uint8_t window_destroy();

/**
 * @brief Roda a janela.
 *
 * @return 0
 * @return 1
 */
extern uint8_t window_run();

/**
 * @brief Retorna o renderer usado pela janela.
 *
 * @return SDL_Renderer*
 */
extern SDL_Renderer *window_get_renderer();

/**
 * @brief Retorna tamanho / posição da janela.
 *
 * @return const struct rect*
 */
extern const struct rect *window_get_placement();

/**
 * @brief Retorna se a janela deve continuar sendo rodada.
 *
 * @return 0
 * @return 1
 */
extern uint8_t window_should_run();

/**
 * @brief Retorna se o painel de configurações está visível.
 *
 * @return 0
 * @return 1
 */
extern uint8_t window_settings_visible();

/**
 * @brief Callback para quando o botão de configuração é clicado.
 *
 */
extern void window_settings_clicked();

#endif
