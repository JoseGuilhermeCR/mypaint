/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef RECT_H_
#define RECT_H_

#include <stdint.h>

struct rect {
    int32_t x;
    int32_t y;
    int32_t w;
    int32_t h;
};

/**
 * @brief Checa se child cabe dentro de parent.
 *
 * @param parent Retângulo "pai".
 * @param child  Retângulo "filho".
 * @return 0
 * @return 1
 */
extern uint8_t rect_fits(const struct rect *parent, const struct rect *child);

/**
 * @brief Checa se retângulo contém ponto.
 *
 * @param rect Retângulo.
 * @param x X
 * @param y Y
 * @return 0
 * @return 1
 */
extern uint8_t rect_contains_point(const struct rect *rect,
                                   int32_t x,
                                   int32_t y);

#endif
