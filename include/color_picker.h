/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef COLOR_PICKER_H_
#define COLOR_PICKER_H_

#include "color.h"
#include "rect.h"
#include "slider.h"

#include <stdint.h>

#include <SDL2/SDL.h>

struct color;

struct color_picker {
    struct rect placement;
    struct rect color_preview_placement;

    struct slider r_slider;
    struct slider g_slider;
    struct slider b_slider;

    struct color color;

    uint8_t is_initialized;
};

/**
 * @brief Inicializa estrutura do color picker.
 *
 * @param cp Ponteiro para estrutura.
 * @param placement Retângulo que indica tamanho / posição do botão.
 * @param parent_bounds Retângulo que indica tamanho / posição do "pai" do
 * botão.
 * @param start_color Ponteiro para cor inicial do color picker.
 * @return 0
 * @return 1
 */
extern uint8_t color_picker_init(struct color_picker *cp,
                                 const struct rect *placement,
                                 const struct rect *parent_bounds,
                                 const struct color *start_color);

/**
 * @brief Destrói o color picker.
 *
 * @param cp Ponteiro para estrutura.
 * @return 0
 * @return 1
 */
extern uint8_t color_picker_destroy(struct color_picker *cp);

/**
 * @brief Renderiza o color picker.
 *
 * @param cp Ponteiro para estrutura.
 * @return 0
 * @return 1
 */
extern uint8_t color_picker_render(struct color_picker *cp);

/**
 * @brief Cuida do input mouse.
 *
 * @param cp Ponteiro para estrutura.
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 * @return 0
 * @return 1
 */
extern uint8_t color_picker_handle_mouse(struct color_picker *cp,
                                         uint32_t state,
                                         int32_t x,
                                         int32_t y);

#endif
