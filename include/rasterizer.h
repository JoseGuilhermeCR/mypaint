/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DDA_H_
#define DDA_H_

#include <stdint.h>

/**
 * @brief Rasteriza uma linha de (x1,y1) a (y2,x2) usando dda.
 *
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 */
extern void rasterize_dda_line(int32_t x1, int32_t y1, int32_t x2, int32_t y2);

/**
 * @brief Rasteriza uma linha de (x1,y1) a (y2,x2) usando bresenham.
 *
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 */
extern void rasterize_bresenham_line(int32_t x1,
                                     int32_t y1,
                                     int32_t x2,
                                     int32_t y2);

/**
 * @brief Rasteriza uma circunferência usando bresenham.
 *
 * @param x
 * @param y
 * @param radius
 */
extern void rasterize_bresenham_circumference(int32_t x,
                                              int32_t y,
                                              uint32_t radius);

#endif
