/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef TRANSFORMS_H_
#define TRANSFORMS_H_

#include <stdint.h>

#define X_AXIS_REFLECTION_BIT ((uint8_t)0x01)
#define Y_AXIS_REFLECTION_BIT ((uint8_t)0x02)

struct list;

/**
 * @brief Aplica translação no canvas.
 *
 * @param lines Lista de linhas no canvas.
 * @param circs Lista de circunferências no canvas.
 * @param off_x Fator de translação no eixo X.
 * @param off_y Fator de translação no eixo Y.
 */
extern void transforms_apply_translation(struct list *lines,
                                         struct list *circs,
                                         int32_t off_x,
                                         int32_t off_y);

/**
 * @brief Aplica rotação no canvas.
 *
 * @param lines Lista de linhas no canvas.
 * @param circs Lista de circunferências no canvas.
 */
extern void transforms_apply_rotation(struct list *lines, struct list *circs);

/**
 * @brief Aplica escala no canvas.
 *
 * @param lines Lista de linhas no canvas.
 * @param circs Lista de circunferências no canvas.
 */
extern void transforms_apply_scaling(struct list *lines, struct list *circs);

/**
 * @brief Aplica reflexão no canvas.
 *
 * @param lines Lista de linhas no canvas.
 * @param circs Lista de circunferências no canvas.
 * @param axis Sobre quais eixos a reflexão será aplicada.
 */
extern void transforms_apply_reflection(struct list *lines,
                                        struct list *circs,
                                        uint8_t axis);

#endif
