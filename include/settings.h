/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SETTINGS_H_
#define SETTINGS_H_

#include "color_picker.h"
#include "rect.h"
#include "slider.h"

/**
 * @brief Inicializa painel de configuração.
 *
 * @param placement Retângulo que indica tamanho / posição do botão.
 * @param parent_bounds Retângulo que indica tamanho / posição do "pai" do
 * botão.
 * @return 0
 * @return 1
 */
extern uint8_t settings_init(const struct rect *placement,
                             const struct rect *parent_bounds);

/**
 * @brief Destrói painel de configuração.
 *
 * @return 0
 * @return 1
 */
extern uint8_t settings_destroy();

/**
 * Renderiza painel de configuração.
 *
 * @return 0
 * @return 1
 */
extern uint8_t settings_render();

/**
 * @brief Cuida de input do mouse.
 *
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 *
 * @return 0
 * @return 1
 */
extern uint8_t settings_handle_mouse(uint32_t state, int32_t x, int32_t y);

/**
 * @brief Retorna a cor de background configurada.
 *
 * @return const struct color* ou NULO caso sistema não esteja iniciado
 */
extern const struct color *settings_get_background_color();

/**
 * @brief Retorna a cor de foreground configurada.
 *
 * @return const struct color* ou NULO caso sistema não esteja iniciado
 */
extern const struct color *settings_get_foreground_color();

/**
 * @brief Retorna o ângulo de rotação em graus configurado.
 *
 * @return int32_t
 */
extern int32_t settings_get_rotation_angle();

/**
 * @brief Retorna o fator de escala no eixo X.
 *
 * @return double
 */
extern double settings_get_scale_factor_x();

/**
 * @brief Retorna o fator de escala no eixo Y.
 *
 * @return double
 */
extern double settings_get_scale_factor_y();

/**
 * @brief Retorna o fator de translação no eixo X.
 *
 * @return int32_t
 */
extern int32_t settings_get_offset_x();

/**
 * @brief Retorna o fator de translação no eixo Y.
 *
 * @return int32_t
 */
extern int32_t settings_get_offset_y();

#endif
