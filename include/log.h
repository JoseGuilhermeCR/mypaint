/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef LOG_H_
#define LOG_H_

#include <stdint.h>

#define LOG_ERROR(fmt, ...) \
    log_print_error(__FILE__, __func__, __LINE__, fmt, ##__VA_ARGS__)

/**
 * @brief Escreve uma mensagem em stderr.
 *
 * @param file Nome do arquivo onde erro ocorreu.
 * @param fn Nome da função onde erro ocorreu.
 * @param line Número da linha onde erro ocorreu.
 * @param fmt FMT para printf.
 * @param ... Parâmetros extras.
 */
extern void log_print_error(const char *file,
                            const char *fn,
                            int32_t line,
                            const char *fmt,
                            ...);

#if defined(DEBUG)

    #define LOG_INFO(fmt, ...) \
        log_print_info(__FILE__, __func__, __LINE__, fmt, ##__VA_ARGS__)

/**
 * @brief Escreve uma mensagem em stdout.
 *
 * @param file Nome do arquivo.
 * @param fn Nome da função.
 * @param line Número da linha.
 * @param fmt FMT para printf.
 * @param ... Parâmetros extras.
 */
extern void log_print_info(const char *file,
                           const char *fn,
                           int32_t line,
                           const char *fmt,
                           ...);

#else

    #define LOG_INFO(fmt, ...)

#endif

#endif
