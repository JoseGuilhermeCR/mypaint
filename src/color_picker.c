/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "color_picker.h"

#include "color.h"
#include "log.h"
#include "slider.h"
#include "window.h"

uint8_t color_picker_init(struct color_picker *cp,
                          const struct rect *placement,
                          const struct rect *parent_bounds,
                          const struct color *start_color)
{
    if (!cp || cp->is_initialized || !placement || !parent_bounds
        || !start_color) {
        return 1;
    }

    if (rect_fits(parent_bounds, placement) != 0) {
        LOG_ERROR("Color picker won't fit in parent_bounds.\n");
        return 1;
    }

    memcpy((void *)&cp->placement,
           (const void *)placement,
           sizeof(struct rect));
    memcpy((void *)&cp->color, (const void *)start_color, sizeof(struct color));

    cp->color_preview_placement.x = cp->placement.x + 20;
    cp->color_preview_placement.y = cp->placement.y + 20;
    cp->color_preview_placement.w = cp->placement.w - 40;
    cp->color_preview_placement.h = 40;

    if (rect_fits(&cp->placement, &cp->color_preview_placement) != 0) {
        LOG_ERROR("Preview won't fit in color_picker.\n");
        return 1;
    }

    struct rect slider_placement = {.x = cp->placement.x + 20,
                                    .y = cp->placement.y + 80,
                                    .w = cp->placement.w - 40,
                                    .h = 20};

    if (slider_init(&cp->r_slider, &slider_placement, &cp->placement) != 0) {
        return 1;
    }

    slider_placement.y += 40;
    if (slider_init(&cp->g_slider, &slider_placement, &cp->placement) != 0) {
        return 1;
    }

    slider_placement.y += 40;
    if (slider_init(&cp->b_slider, &slider_placement, &cp->placement) != 0) {
        return 1;
    }

    slider_set_min_max(&cp->r_slider, 0, 255);
    slider_set_min_max(&cp->g_slider, 0, 255);
    slider_set_min_max(&cp->b_slider, 0, 255);
    slider_set_value(&cp->r_slider, cp->color.r);
    slider_set_value(&cp->g_slider, cp->color.g);
    slider_set_value(&cp->b_slider, cp->color.b);

    struct color slider_color = {.r = 0xFF, .g = 0x00, .b = 0x00};
    slider_set_bar_color(&cp->r_slider, &slider_color);

    slider_color.r = 0;
    slider_color.g = 0xFF;
    slider_set_bar_color(&cp->g_slider, &slider_color);

    slider_color.g = 0;
    slider_color.b = 0xFF;
    slider_set_bar_color(&cp->b_slider, &slider_color);

    cp->is_initialized = 1;

    LOG_INFO("Creating color picker with %iw %ih %ix %iy.\n",
             cp->placement.w,
             cp->placement.h,
             cp->placement.x,
             cp->placement.y);
    return 0;
}

uint8_t color_picker_destroy(struct color_picker *cp)
{
    if (!cp || !cp->is_initialized) {
        return 1;
    }

    memset((void *)cp, 0, sizeof(struct color_picker));
    return 0;
}

uint8_t color_picker_render(struct color_picker *cp)
{
    if (!cp || !cp->is_initialized) {
        return 1;
    }

    SDL_Renderer *renderer = window_get_renderer();

    SDL_Rect rect;

    SDL_SetRenderDrawColor(renderer,
                           COLOR_PICKER_DEFAULT_BG_COLOR.r,
                           COLOR_PICKER_DEFAULT_BG_COLOR.g,
                           COLOR_PICKER_DEFAULT_BG_COLOR.b,
                           0xFF);
    memcpy((void *)&rect, (const void *)&cp->placement, sizeof(SDL_Rect));
    SDL_RenderFillRect(renderer, &rect);

    SDL_SetRenderDrawColor(renderer,
                           cp->color.r,
                           cp->color.g,
                           cp->color.b,
                           0xFF);
    memcpy((void *)&rect,
           (const void *)&cp->color_preview_placement,
           sizeof(SDL_Rect));
    SDL_RenderFillRect(renderer, &rect);

    slider_render(&cp->r_slider);
    slider_render(&cp->g_slider);
    slider_render(&cp->b_slider);

    return 0;
}

uint8_t color_picker_handle_mouse(struct color_picker *cp,
                                  uint32_t state,
                                  int32_t x,
                                  int32_t y)
{
    if (!cp || !cp->is_initialized) {
        return 1;
    }

    if (rect_contains_point(&cp->placement, x, y) != 0) {
        return 1;
    }

    slider_handle_mouse(&cp->r_slider, state, x, y);
    slider_handle_mouse(&cp->g_slider, state, x, y);
    slider_handle_mouse(&cp->b_slider, state, x, y);

    cp->color.r = cp->r_slider.value;
    cp->color.g = cp->g_slider.value;
    cp->color.b = cp->b_slider.value;

    return 0;
}
