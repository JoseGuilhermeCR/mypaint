/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "rasterizer.h"

#include "canvas.h"
#include "min_max.h"
#include "rect.h"

/**
 * @brief Rasteriza uma linha vertical, sem nenhum algoritmo, para otimização.
 *
 * @param x
 * @param y1
 * @param y2
 */
static void vertical_line(int32_t x, int32_t y1, int32_t y2);

/**
 * @brief Rasteriza uma linha horizontal, sem nenhum algoritmo, para otimização.
 *
 * @param y
 * @param x1
 * @param x2
 */
static void horizontal_line(int32_t y, int32_t x1, int32_t x2);

/**
 * @brief Coloca os pontos simétricos de uma circunferência no canvas.
 *
 * @param cx Centro X.
 * @param cy Centro Y.
 * @param x X em que será usado simetria.
 * @param y Y em que será usado simetria.
 */
static void draw_symmetric_points(int32_t cx, int32_t cy, int32_t x, int32_t y);

void rasterize_dda_line(int32_t x1, int32_t y1, int32_t x2, int32_t y2)
{
    if (x1 == x2 && y1 != y2) {
        // Linha vertical. Não há necessidade para algoritmo.
        vertical_line(x1, y1, y2);
        return;
    } else if (x1 != x2 && y1 == y2) {
        // Linha horizontal. Não há necessidade para algoritmo.
        horizontal_line(y1, x1, x2);
        return;
    }

    const int32_t dx = (int32_t)x2 - (int32_t)x1;
    const int32_t dy = (int32_t)y2 - (int32_t)y1;

    const uint32_t steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    const double x_inc = dx / (double)steps;
    const double y_inc = dy / (double)steps;

    double x = x1;
    double y = y1;

    for (uint32_t u = 0; u <= steps; ++u) {
        canvas_set_pixel_coord(round(x), round(y));
        x += x_inc;
        y += y_inc;
    }
}

void rasterize_bresenham_line(int32_t x1, int32_t y1, int32_t x2, int32_t y2)
{
    if (x1 == x2 && y1 != y2) {
        // Linha vertical. Não há necessidade para algoritmo.
        vertical_line(x1, y1, y2);
        return;
    } else if (x1 != x2 && y1 == y2) {
        // Linha horizontal. Não há necessidade para algoritmo.
        horizontal_line(y1, x1, x2);
        return;
    }

    const int32_t dx = abs((int32_t)x2 - (int32_t)x1);
    const int32_t sx = (x1 < x2) ? 1 : -1;

    const int32_t dy = -abs((int32_t)y2 - (int32_t)y1);
    const int32_t sy = (y1 < y2) ? 1 : -1;

    int32_t err = dx + dy;
    while (1) {
        canvas_set_pixel_coord(x1, y1);

        if (x1 == x2 && y1 == y2) {
            break;
        }

        const int32_t e2 = 2 * err;
        if (e2 >= dy) {
            err += dy;
            x1 += sx;
        }

        if (e2 <= dx) {
            err += dx;
            y1 += sy;
        }
    }
}

void rasterize_bresenham_circumference(int32_t x, int32_t y, uint32_t radius)
{
    const int32_t center_x = x;
    const int32_t center_y = y;

    x = 0;
    y = radius;
    draw_symmetric_points(center_x, center_y, x, y);

    // Parâmetro de decisão.
    int32_t p = 3 - (2 * radius);
    while (y >= x) {
        ++x;
        if (p > 0) {
            --y;
            p = p + 4 * (x - y) + 10;
        } else {
            p = p + 4 * x + 6;
        }
        draw_symmetric_points(center_x, center_y, x, y);
    }
}

static void vertical_line(int32_t x, int32_t y1, int32_t y2)
{
    int32_t start     = min(y1, y2);
    const int32_t end = max(y1, y2);

    while (start != end) {
        canvas_set_pixel_coord(x, start++);
    }
}

static void horizontal_line(int32_t y, int32_t x1, int32_t x2)
{
    int32_t start     = min(x1, x2);
    const int32_t end = max(x1, x2);

    while (start != end) {
        canvas_set_pixel_coord(start++, y);
    }
}

static void draw_symmetric_points(int32_t cx, int32_t cy, int32_t x, int32_t y)
{
    canvas_set_pixel_coord(cx + x, cy + y);
    canvas_set_pixel_coord(cx - x, cy + y);
    canvas_set_pixel_coord(cx + x, cy - y);
    canvas_set_pixel_coord(cx - x, cy - y);
    canvas_set_pixel_coord(cx + y, cy + x);
    canvas_set_pixel_coord(cx - y, cy + x);
    canvas_set_pixel_coord(cx + y, cy - x);
    canvas_set_pixel_coord(cx - y, cy - x);
}
