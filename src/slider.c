/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "slider.h"

#include "log.h"
#include "window.h"

#include <math.h>
#include <string.h>

/**
 * @brief Cuida do movimento de drag de um slider.
 *
 * @param s Ponteiro para slider.
 * @param state Estado do mouse.
 * @param x Posição X do mouse.
 */
static void handle_drag(struct slider *s, uint32_t state, int32_t x);

/**
 * @brief Mapeia um número do alcance (old_start, old_end) para (new_start,
 * new_end).
 *
 * @param value
 * @param old_start
 * @param old_end
 * @param new_start
 * @param new_end
 * @return double
 */
static double map(double value,
                  double old_start,
                  double old_end,
                  double new_start,
                  double new_end);

uint8_t slider_init(struct slider *s,
                    const struct rect *placement,
                    const struct rect *parent_bounds)
{
    if (!s || s->is_initialized || !placement || !parent_bounds) {
        return 1;
    }

    if (rect_fits(parent_bounds, placement) != 0) {
        LOG_ERROR("slider won't fit in parent bounds.\n");
        return 1;
    }

    memcpy((void *)&s->placement, (const void *)placement, sizeof(struct rect));

    s->min   = 0;
    s->max   = 100;
    s->value = 0;

    s->cursor_placement.x = s->placement.x;
    s->cursor_placement.y = s->placement.y + 5;
    s->cursor_placement.w = 5;
    s->cursor_placement.h = ((int32_t)s->placement.h / 1.5f);

    s->bar_placement.w = s->placement.w - 10;
    s->bar_placement.h = s->placement.h / 4;
    s->bar_placement.x = s->placement.x;
    s->bar_placement.y =
        s->placement.y + s->placement.h / 2 - s->bar_placement.h / 2;

    s->bar_color.r = 0x00;
    s->bar_color.g = 0x00;
    s->bar_color.b = 0x00;

    s->is_initialized = 1;

    LOG_INFO("Creating slider with %iw %ih %ix %iy.\n",
             s->placement.w,
             s->placement.h,
             s->placement.x,
             s->placement.y);
    return 0;
}

uint8_t slider_destroy(struct slider *s)
{
    if (!s || !s->is_initialized) {
        return 1;
    }

    memset((void *)s, 0, sizeof(struct slider));
    return 0;
}

uint8_t slider_render(struct slider *s)
{
    if (!s || !s->is_initialized) {
        return 1;
    }

    SDL_Renderer *renderer = window_get_renderer();

    SDL_Rect rect;
    SDL_SetRenderDrawColor(renderer,
                           s->bar_color.r,
                           s->bar_color.g,
                           s->bar_color.b,
                           0xFF);
    memcpy((void *)&rect, (const void *)&s->bar_placement, sizeof(SDL_Rect));
    SDL_RenderFillRect(renderer, &rect);

    if (s->state == SLIDER_STATE_NOT_SELECTED) {
        SDL_SetRenderDrawColor(renderer, 0x30, 0x30, 0x30, 0xFF);
    } else {
        SDL_SetRenderDrawColor(renderer, 0xB0, 0xB0, 0xB0, 0xFF);
    }

    memcpy((void *)&rect, (const void *)&s->cursor_placement, sizeof(SDL_Rect));
    SDL_RenderFillRect(renderer, &rect);

    return 0;
}

uint8_t slider_handle_mouse(struct slider *s,
                            uint32_t state,
                            int32_t x,
                            int32_t y)
{
    if (!s || !s->is_initialized) {
        return 1;
    }

    if (rect_contains_point(&s->placement, x, y) != 0) {
        return 1;
    }

    if (rect_contains_point(&s->cursor_placement, x, y) == 0) {
        switch (s->state) {
            case SLIDER_STATE_NOT_SELECTED:
                s->state = SLIDER_STATE_SELECTED;
                break;
            case SLIDER_STATE_SELECTED:
                if ((state & SDL_BUTTON_LMASK) != 0) {
                    s->state = SLIDER_STATE_DRAG;
                }
                break;
            case SLIDER_STATE_DRAG:
                handle_drag(s, state, x);
                break;
            default:
                __builtin_unreachable();
        }
    } else {
        if (s->state == SLIDER_STATE_DRAG) {
            handle_drag(s, state, x);
        } else {
            s->state = SLIDER_STATE_NOT_SELECTED;
        }
    }

    return 0;
}

uint8_t slider_set_min_max(struct slider *s, int32_t min, int32_t max)
{
    if (!s || !s->is_initialized) {
        return 1;
    }

    s->min = min;
    s->max = max;

    return 0;
}

uint8_t slider_set_value(struct slider *s, int32_t value)
{
    if (!s || !s->is_initialized) {
        return 1;
    }

    if (value < s->min || value > s->max || s->max == 0) {
        return 1;
    }

    s->value               = value;
    const double percent   = (double)value / s->max;
    const int32_t cursor_x = (int32_t)round(percent * s->bar_placement.w);

    s->cursor_placement.x = s->bar_placement.x + cursor_x;
    return 0;
}

uint8_t slider_set_bar_color(struct slider *s, const struct color *color)
{
    if (!s || !s->is_initialized) {
        return 1;
    }

    memcpy((void *)&s->bar_color, (const void *)color, sizeof(struct color));
    return 0;
}

static void handle_drag(struct slider *s, uint32_t state, int32_t x)
{
    if ((state & SDL_BUTTON_LMASK) != 0) {
        s->cursor_placement.x = x - (s->cursor_placement.w / 2);
        if (s->cursor_placement.x < s->bar_placement.x) {
            s->cursor_placement.x = s->placement.x;
        } else if (s->cursor_placement.x
                   > s->bar_placement.x + s->bar_placement.w) {
            s->cursor_placement.x = s->bar_placement.x + s->bar_placement.w;
        }
    } else {
        s->state = SLIDER_STATE_SELECTED;
    }

    const int32_t cursor_x = s->cursor_placement.x - s->bar_placement.x;
    const double percent   = (double)cursor_x / (double)s->bar_placement.w;
    s->value =
        (int32_t)round(map(percent, 0.0, 1.0, (double)s->min, (double)s->max));
}

static double map(double value,
                  double old_start,
                  double old_end,
                  double new_start,
                  double new_end)
{
    const double new_value =
        (value - old_start) / (old_end - old_start) * (new_end - new_start)
        + new_start;
    return new_value;
}
