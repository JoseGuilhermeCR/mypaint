/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "window.h"

#include "canvas.h"
#include "font.h"
#include "log.h"
#include "settings.h"
#include "toolbox.h"

#include <string.h>

#include <SDL2/SDL.h>

#define FRAME_DELAY_MS ((uint32_t)16)

struct window {
    SDL_Window *sdl_window;
    SDL_Renderer *sdl_renderer;
    struct rect placement;

    uint32_t last_time;
    uint32_t current_time;

    uint8_t settings_visible;
    uint8_t should_run;
    uint8_t is_initialized;
};

/**
 * @brief Desabilita painel de configurações.
 *
 */
static void disable_settings();

static struct window window;

uint8_t window_init()
{
    // Não queremos criar outra janela se já tiver sido criada.
    if (window.is_initialized) {
        return 1;
    }

    window.sdl_window = SDL_CreateWindow(
        "Trabalho C.G. - José Guilherme de C. Rodrigues - 651201",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        0,
        0,
        SDL_WINDOW_HIDDEN);

    if (!window.sdl_window) {
        LOG_ERROR("Not able to create window: %s\n", SDL_GetError());
        return 1;
    }

    const int32_t index = SDL_GetWindowDisplayIndex(window.sdl_window);
    if (index < 0) {
        LOG_ERROR("Not able to get display index: %s\n", SDL_GetError());
        goto post_window_error;
    }

    SDL_Rect usable_bounds;
    if (SDL_GetDisplayUsableBounds(index, &usable_bounds) < 0) {
        LOG_ERROR("Not able to get usable bounds: %s\n", SDL_GetError());
        goto post_window_error;
    }

    LOG_INFO("Display %i usable bounds: %iw %ih %ix %iy.\n",
             index,
             usable_bounds.w,
             usable_bounds.h,
             usable_bounds.x,
             usable_bounds.y);

    // Temos o tamanho da tela, vamos tentar calcular um tamanho legal para o
    // resto da aplicação.
    if (usable_bounds.w > WINDOW_DESIRED_WIDTH
        && usable_bounds.h > WINDOW_DESIRED_HEIGHT) {
        window.placement.w = WINDOW_DESIRED_WIDTH;
        window.placement.h = WINDOW_DESIRED_HEIGHT;
    } else {
        LOG_ERROR("Screen is not big enough!\n");
        // Calculando a porcentagem que a tela ideal teria e então usando a
        // mesma porcentagem nessa tela menor.
        const double width_percent =
            (double)WINDOW_DESIRED_WIDTH / (double)WINDOW_IDEAL_MONITOR_WIDTH;
        const double height_percent =
            (double)WINDOW_DESIRED_HEIGHT / (double)WINDOW_IDEAL_MONITOR_HEIGHT;

        window.placement.w = (int32_t)(usable_bounds.w * width_percent);
        window.placement.h = (int32_t)(usable_bounds.h * height_percent);
    }

    window.placement.x = 0;
    window.placement.y = 0;

    SDL_SetWindowSize(window.sdl_window,
                      window.placement.w,
                      window.placement.h);
    SDL_SetWindowPosition(window.sdl_window,
                          SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED);
    SDL_ShowWindow(window.sdl_window);

    window.sdl_renderer =
        SDL_CreateRenderer(window.sdl_window, -1, SDL_RENDERER_ACCELERATED);
    if (!window.sdl_renderer) {
        goto post_window_error;
    }

    window.settings_visible = 0;
    window.should_run       = 1;
    window.is_initialized   = 1;
    return 0;
post_window_error:
    SDL_DestroyWindow(window.sdl_window);
    return 1;
}

uint8_t window_destroy()
{
    if (!window.is_initialized) {
        return 1;
    }

    SDL_DestroyRenderer(window.sdl_renderer);
    SDL_DestroyWindow(window.sdl_window);
    memset((void *)&window, 0, sizeof(struct window));
    return 0;
}

uint8_t window_run()
{
    if (!window.is_initialized) {
        return 1;
    }

    window.current_time = SDL_GetTicks();

    // Registramos eventos o tempo todo. Não queremos
    // colocar delay e atrapalhar a responsividade.
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            window.should_run = 0;
        }

        if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
            if (!window.settings_visible) {
                canvas_quit_state();
            } else {
                disable_settings();
            }
        }
    }

    int32_t x;
    int32_t y;
    const uint32_t state = SDL_GetMouseState(&x, &y);

    // Canvas e Settings ocupam a mesma porção da tela.
    if (!window.settings_visible) {
        canvas_handle_mouse(state, x, y);
    } else {
        settings_handle_mouse(state, x, y);
    }

    toolbox_handle_mouse(state, x, y);

    // Renderização feita a cada FRAME_DELAY_MS, se tudo der certo e nenhuma
    // outra parte do código tiver bloqueado por mais que esse tempo.
    if (window.current_time >= window.last_time + FRAME_DELAY_MS) {
        SDL_SetRenderDrawColor(window.sdl_renderer, 0x00, 0x00, 0x00, 0x00);
        SDL_RenderClear(window.sdl_renderer);

        canvas_render();
        if (window.settings_visible) {
            settings_render();
        }
        toolbox_render();

        SDL_RenderPresent(window.sdl_renderer);
        window.last_time = window.current_time;
        return 0;
    }

    return 1;
}

SDL_Renderer *window_get_renderer()
{
    if (!window.is_initialized) {
        return NULL;
    }

    return window.sdl_renderer;
}

const struct rect *window_get_placement()
{
    if (!window.is_initialized) {
        return NULL;
    }

    return &window.placement;
}

uint8_t window_should_run()
{
    return window.should_run;
}

uint8_t window_settings_visible()
{
    return window.settings_visible;
}

void window_settings_clicked()
{
    if (!window.is_initialized) {
        return;
    }

    if (!window.settings_visible) {
        window.settings_visible = 1;
    } else {
        disable_settings();
    }
}

static void disable_settings()
{
    canvas_set_bg_color(settings_get_background_color());
    canvas_set_fg_color(settings_get_foreground_color());
    toolbox_set_current_mode_text(
        font_get_text_with_id(NOTHING_SELECTED_TEXT_ID));
    window.settings_visible = 0;
}
