/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "color.h"

const struct color CANVAS_DEFAULT_BG_COLOR   = {0x10, 0x10, 0x10};
const struct color CANVAS_DEFAULT_FG_COLOR   = {0xFF, 0xFF, 0xFF};
const struct color CANVAS_DEFAULT_RECT_COLOR = {0xFF, 0x00, 0x00};
const struct color CANVAS_DEFAULT_AXIS_COLOR = {0x00, 0xFF, 0x00};

const struct color TOOLBOX_DEFAULT_BG_COLOR = {0x50, 0x50, 0x50};

const struct color BUTTON_DEFAULT_BG_COLOR       = {0x70, 0x70, 0x70};
const struct color BUTTON_DEFAULT_BG_HOVER_COLOR = {0x00, 0x80, 0x00};
const struct color BUTTON_DEFAULT_FG_COLOR       = {0x00, 0x00, 0x00};

const struct color BUTTON_DEFAULT_BORDER_COLOR = {0x10, 0x10, 0x10};

const struct color SETTINGS_DEFAULT_BG_COLOR     = {0x80, 0x80, 0x80};
const struct color COLOR_PICKER_DEFAULT_BG_COLOR = {0x90, 0x90, 0x90};
