/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "button.h"
#include "canvas.h"
#include "color.h"
#include "font.h"
#include "log.h"
#include "rect.h"
#include "settings.h"
#include "toolbox.h"
#include "window.h"

#include <stdint.h>
#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define NUMBER_OF_STACKED_BUTTONS ((uint8_t)16)

/**
 * @brief Inicializa as texturas dos numerais que serão usados.
 * Nós estamos criando mais texturas do que precisamos... mas dessa
 * maneira fica mais simples.
 *
 * @return 0
 * @return 1
 */
static uint8_t initialize_number_texts();

/**
 * @brief Inicializa os botões, seus callbacks e posições.
 *
 * @return 0
 * @return 1
 */
static uint8_t initialize_buttons();

static struct button button_translation_positive_x;
static struct button button_translation_negative_x;
static struct button button_translation_positive_y;
static struct button button_translation_negative_y;
static struct button button_rotation;
static struct button button_scale;
static struct button button_reflection_x;
static struct button button_reflection_y;
static struct button button_reflection_x_y;
static struct button button_dda_line;
static struct button button_bresenham_line;
static struct button button_bresenham_circ;
static struct button button_cohen_sutherland;
static struct button button_liang_barsky;
static struct button button_clean_canvas;
static struct button button_toggle_axis;
static struct button button_settings;

#if defined(__WIN32)
// No windows, a função main é contida pela biblioteca do SDL.
// Eles fazem um define e a nossa main vira SDL_Main.
// Isso por vários motivos e o principal deles... é claro... Windows!
// Então aqui trocamos a assinatura da função para que bata com a esperada
// pelo SDL que chamará a nossa main.
int32_t main(int32_t argc, char *argv[])
#else
int32_t main(int32_t argc, const char *const argv[])
#endif
{
    (void)argc;
    (void)argv;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        LOG_ERROR("Video initialization failed: %s\n", SDL_GetError());
        goto sdl_init_error;
    }

    if (window_init() != 0) {
        LOG_ERROR("Window init failed.\n");
        goto window_init_error;
    }

    const struct rect *window_placement = window_get_placement();

    if (font_init() != 0) {
        goto font_init_error;
    }

    initialize_number_texts();

    const struct rect canvas_placement = {.x = 190,
                                          .y = 10,
                                          .w = window_placement->w - 200,
                                          .h = window_placement->h - 20};

    if (canvas_init(&canvas_placement, window_placement) != 0) {
        goto canvas_init_error;
    }

    const struct rect settings_placement = {.x = 190,
                                            .y = 10,
                                            .w = canvas_placement.w / 2,
                                            .h = canvas_placement.h};

    if (settings_init(&settings_placement, window_placement) != 0) {
        goto settings_init_error;
    }

    const struct rect toolbox_placement = {.x = 5,
                                           .y = 10,
                                           .w = canvas_placement.x - 10,
                                           .h = canvas_placement.h};

    if (toolbox_init(&toolbox_placement, window_placement) != 0) {
        goto toolbox_init_error;
    }

    if (initialize_buttons() != 0) {
        LOG_ERROR("Could not initialize buttons.\n");
        goto button_init_error;
    }

    while (window_should_run() == 1) {
        window_run();
    }

    toolbox_destroy();
    canvas_destroy();

    font_destroy();
    window_destroy();

    LOG_INFO("Exiting...\n");
    SDL_Quit();
    return 0;
button_init_error:
font_init_error:
toolbox_init_error:
    settings_destroy();
settings_init_error:
    canvas_destroy();
canvas_init_error:
    window_destroy();
window_init_error:
    SDL_Quit();
sdl_init_error:
    return 1;
}

static uint8_t initialize_number_texts()
{
    char buffer[10];
    for (int32_t i = -NEGATIVE_NUMERAL_TEXTURE_COUNT;
         i != POSITIVE_NUMERAL_TEXTURE_COUNT;
         ++i) {
        snprintf(buffer, sizeof(buffer), "%i", i);
        if (font_create_texture(buffer,
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                NEGATIVE_NUMERAL_TEXTURE_COUNT + i)
            == -1) {
            return 1;
        }
    }

    return 0;
}

static uint8_t initialize_buttons()
{
    const uint32_t y_offset              = 2;
    const struct rect *toolbox_placement = toolbox_get_placement();

    const double button_height =
        ((double)toolbox_placement->h - (y_offset * NUMBER_OF_STACKED_BUTTONS)
         - 20.0)
        / (double)NUMBER_OF_STACKED_BUTTONS;

    struct rect joystick_placement = {.x = toolbox_placement->w / 2 - 20,
                                      .y = toolbox_placement->y + y_offset,
                                      .w = 50,
                                      .h = button_height};

    uint8_t errors = 0;
    errors += button_init(&button_translation_negative_y,
                          &joystick_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_translation_negative_y,
        font_get_text_with_id(font_create_texture("Y-",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_translation_negative_y,
                                  canvas_translate_negative_y);
    errors += toolbox_attach_button(&button_translation_negative_y);

    joystick_placement.x = 20;
    joystick_placement.y += button_height;

    errors += button_init(&button_translation_negative_x,
                          &joystick_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_translation_negative_x,
        font_get_text_with_id(font_create_texture("X-",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_translation_negative_x,
                                  canvas_translate_negative_x);
    errors += toolbox_attach_button(&button_translation_negative_x);

    joystick_placement.x += joystick_placement.w * 2;

    errors += button_init(&button_translation_positive_x,
                          &joystick_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_translation_positive_x,
        font_get_text_with_id(font_create_texture("X+",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_translation_positive_x,
                                  canvas_translate_positive_x);
    errors += toolbox_attach_button(&button_translation_positive_x);

    joystick_placement.x = toolbox_placement->w / 2 - 20;
    joystick_placement.y += button_height;

    errors += button_init(&button_translation_positive_y,
                          &joystick_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_translation_positive_y,
        font_get_text_with_id(font_create_texture("Y+",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_translation_positive_y,
                                  canvas_translate_positive_y);
    errors += toolbox_attach_button(&button_translation_positive_y);

    struct rect button_placement = {.x = 15,
                                    .y = joystick_placement.y + y_offset,
                                    .w = toolbox_placement->w - 20,
                                    .h = button_height};

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_rotation,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_rotation,
        font_get_text_with_id(font_create_texture("Rotação",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_rotation, canvas_rotate);
    errors += toolbox_attach_button(&button_rotation);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_scale,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_scale,
        font_get_text_with_id(font_create_texture("Escala",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_scale, canvas_scale);
    errors += toolbox_attach_button(&button_scale);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_reflection_x,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_reflection_x,
        font_get_text_with_id(font_create_texture("Reflexão X",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_reflection_x, canvas_reflect_x);
    errors += toolbox_attach_button(&button_reflection_x);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_reflection_y,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_reflection_y,
        font_get_text_with_id(font_create_texture("Reflexão Y",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_reflection_y, canvas_reflect_y);
    errors += toolbox_attach_button(&button_reflection_y);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_reflection_x_y,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_reflection_x_y,
        font_get_text_with_id(font_create_texture("Reflexão X/Y",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_reflection_x_y, canvas_reflect_xy);
    errors += toolbox_attach_button(&button_reflection_x_y);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_dda_line,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER | IS_MODE_BUTTON);
    errors += button_set_text(
        &button_dda_line,
        font_get_text_with_id(font_create_texture("Linha DDA",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors +=
        button_set_callback(&button_dda_line, canvas_set_draw_dda_line_state);
    errors += toolbox_attach_button(&button_dda_line);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_bresenham_line,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER | IS_MODE_BUTTON);
    errors += button_set_text(
        &button_bresenham_line,
        font_get_text_with_id(font_create_texture("Linha Bresenham",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_bresenham_line,
                                  canvas_set_draw_bresenham_line_state);
    errors += toolbox_attach_button(&button_bresenham_line);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_bresenham_circ,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER | IS_MODE_BUTTON);
    errors += button_set_text(
        &button_bresenham_circ,
        font_get_text_with_id(font_create_texture("Circ. Bresenham",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors +=
        button_set_callback(&button_bresenham_circ,
                            canvas_set_draw_bresenham_circumference_state);
    errors += toolbox_attach_button(&button_bresenham_circ);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_cohen_sutherland,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER | IS_MODE_BUTTON);
    errors += button_set_text(
        &button_cohen_sutherland,
        font_get_text_with_id(font_create_texture("Cohen Sutherland",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_cohen_sutherland,
                                  canvas_set_cohen_sutherland_state);
    errors += toolbox_attach_button(&button_cohen_sutherland);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_liang_barsky,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER | IS_MODE_BUTTON);
    errors += button_set_text(
        &button_liang_barsky,
        font_get_text_with_id(font_create_texture("Liang Barsky",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_liang_barsky,
                                  canvas_set_liang_barsky_state);
    errors += toolbox_attach_button(&button_liang_barsky);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_clean_canvas,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_clean_canvas,
        font_get_text_with_id(font_create_texture("Limpar Canvas",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_clean_canvas, canvas_clear);
    errors += toolbox_attach_button(&button_clean_canvas);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_toggle_axis,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER);
    errors += button_set_text(
        &button_toggle_axis,
        font_get_text_with_id(font_create_texture("Ligar/Desligar Eixos",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_toggle_axis, canvas_toggle_axis);
    errors += toolbox_attach_button(&button_toggle_axis);

    button_placement.y += button_placement.h + y_offset;
    errors += button_init(&button_settings,
                          &button_placement,
                          toolbox_placement,
                          HAS_HOVER_COLOR | HAS_BORDER | IS_MODE_BUTTON);
    errors += button_set_text(
        &button_settings,
        font_get_text_with_id(font_create_texture("Configurações",
                                                  FONT_SIZE_MEDIUM,
                                                  BUTTON_DEFAULT_FG_COLOR,
                                                  -1)));
    errors += button_set_callback(&button_settings, window_settings_clicked);
    errors += toolbox_attach_button(&button_settings);

    LOG_INFO("errors: %i\n", errors);
    return errors;
}
