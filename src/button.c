/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "button.h"

#include "color.h"
#include "log.h"
#include "toolbox.h"
#include "window.h"

#include <stdlib.h>
#include <string.h>

#define CLICK_DEBOUNCE_TIME_MS ((uint32_t)200)

uint8_t button_init(struct button *btn,
                    const struct rect *placement,
                    const struct rect *parent_bounds,
                    uint8_t options)
{
    if (!btn || btn->is_initialized || !placement) {
        return 1;
    }

    if (rect_fits(parent_bounds, placement) != 0) {
        LOG_ERROR("Button won't fit in parent_bounds.\n");
        return 1;
    }

    memcpy((void *)&btn->placement,
           (const void *)placement,
           sizeof(struct rect));

    btn->callback = NULL;

    btn->debounce_timer = 0;
    btn->is_initialized = 1;
    btn->is_hovered     = 0;
    btn->options        = options;

    return 0;
}

uint8_t button_render(struct button *btn)
{
    if (!btn || !btn->is_initialized) {
        return 1;
    }

    SDL_Renderer *renderer = window_get_renderer();

    struct color bg =
        ((btn->options & HAS_HOVER_COLOR) == 0 || !btn->is_hovered)
            ? BUTTON_DEFAULT_BG_COLOR
            : BUTTON_DEFAULT_BG_HOVER_COLOR;

    // Renderiza borda, se existir.
    if ((btn->options & HAS_BORDER) != 0) {
        SDL_SetRenderDrawColor(renderer,
                               BUTTON_DEFAULT_BORDER_COLOR.r,
                               BUTTON_DEFAULT_BORDER_COLOR.g,
                               BUTTON_DEFAULT_BORDER_COLOR.b,
                               0xFF);

        // FIXME: SDL_RenderDrawRect está terminando o retângulo de maneira
        // desalinhada. Nesse caso, estou dando fill em um retângulo inteiro que
        // será tampado pelo botão em si e dará o efeito de borda, assim ela não
        // fica desalinhada. Consertar depois...
        SDL_Rect border_rect = {.x = btn->placement.x,
                                .y = btn->placement.y,
                                .w = btn->placement.w,
                                .h = btn->placement.h};
        SDL_RenderFillRect(renderer, &border_rect);
    }

    // Renderiza botão.
    SDL_SetRenderDrawColor(renderer, bg.r, bg.g, bg.b, 0xFF);

    // FIXME: Por conta do problema acima, temos de diminuir a área normal do
    // botão (a que não é de borda)... Para que a borda desenhada anteriormente
    // fique visível.
    SDL_Rect button_rect = {.x = btn->placement.x + 1,
                            .y = btn->placement.y + 1,
                            .w = btn->placement.w - 2,
                            .h = btn->placement.h - 2};

    SDL_RenderFillRect(renderer, &button_rect);

    // Renderiza texto, se houver algum.
    if (btn->text) {
        SDL_Rect *dest = (SDL_Rect *)&btn->text_placement;
        SDL_RenderCopy(renderer, btn->text->texture, NULL, dest);
    }

    return 0;
}

uint8_t button_destroy(struct button *btn)
{
    if (!btn || !btn->is_initialized) {
        return 1;
    }

    memset((void *)btn, 0, sizeof(struct button));
    return 0;
}

uint8_t button_handle_mouse(struct button *btn,
                            uint32_t state,
                            int32_t x,
                            int32_t y)
{
    if (!btn || !btn->is_initialized) {
        return 1;
    }

    btn->is_hovered = 0;
    if (rect_contains_point(&btn->placement, x, y) != 0) {
        return 1;
    }

    // Botão sendo pressionado.
    if ((state & SDL_BUTTON_LMASK) != 0
        && SDL_GetTicks() > btn->debounce_timer + CLICK_DEBOUNCE_TIME_MS) {
        if (btn->callback) {
            if ((btn->options & IS_MODE_BUTTON) != 0 && btn->text) {
                toolbox_set_current_mode_text(btn->text);
            }

            btn->callback();
            btn->debounce_timer = SDL_GetTicks();
        }
    }

    btn->is_hovered = 1;
    return 0;
}

uint8_t button_set_callback(struct button *btn, void (*callback)(void *))
{
    if (!btn || !btn->is_initialized) {
        return 1;
    }

    btn->callback = callback;
    return 0;
}

uint8_t button_set_text(struct button *btn, const struct text *text)
{
    if (!btn || !btn->is_initialized || !text) {
        return 1;
    }

    btn->text = text;

    // Já faz a conta para que o texto fique no meio do botão.
    btn->text_placement.w = btn->text->w;
    btn->text_placement.h = btn->text->h;
    btn->text_placement.x =
        btn->placement.x + (btn->placement.w / 2) - (btn->text->w / 2);
    btn->text_placement.y =
        btn->placement.y + (btn->placement.h / 2) - (btn->text->h / 2);

    return 0;
}
