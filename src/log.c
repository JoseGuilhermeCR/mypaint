/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "log.h"

#include <stdarg.h>
#include <stdio.h>

#include <SDL2/SDL.h>

/**
 * @brief Escreve uma mensagem em um tty.
 *
 * @param tty Descritor de terminal.
 * @param type String que indica o tipo de log.
 * @param file Nome do arquivo.
 * @param fn Nome da função.
 * @param line Número da linha.
 * @param fmt FMT.
 * @param fmt_list Lista de parâmetros.
 */
static void log_print(FILE *tty,
                      const char *type,
                      const char *file,
                      const char *fn,
                      int32_t line,
                      const char *fmt,
                      va_list *fmt_list)
{
    fprintf(tty,
            "(%s) %s:%s:%i [%u] -> ",
            type,
            file,
            fn,
            line,
            SDL_GetTicks());
    vfprintf(tty, fmt, *fmt_list);
}

void log_print_error(const char *file,
                     const char *fn,
                     int32_t line,
                     const char *fmt,
                     ...)
{
    va_list fmt_list;
    va_start(fmt_list, fmt);

    log_print(stderr, "ERROR", file, fn, line, fmt, &fmt_list);

    va_end(fmt_list);
}

void log_print_info(const char *file,
                    const char *fn,
                    int32_t line,
                    const char *fmt,
                    ...)
{
    va_list fmt_list;
    va_start(fmt_list, fmt);

    log_print(stdout, "INFO", file, fn, line, fmt, &fmt_list);

    va_end(fmt_list);
}
