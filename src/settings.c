/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "settings.h"

#include "canvas.h"
#include "color.h"
#include "color_picker.h"
#include "font.h"
#include "log.h"
#include "slider.h"
#include "window.h"

struct settings {
    struct rect placement;

    struct color_picker foreground_picker;
    struct color_picker background_picker;
    struct slider angle_slider;
    struct slider scale_factor_x;
    struct slider scale_factor_y;
    struct slider offset_x;
    struct slider offset_y;
};

static struct settings settings;

static struct text *background_color_text;
static struct text *foreground_color_text;

static struct text *rotation_angle_text;
static struct text *rotation_var_angle_text;

static struct text *scale_factor_x_text;
static struct text *scale_factor_x_var_text;
static struct text *scale_factor_y_text;
static struct text *scale_factor_y_var_text;

static struct text *offset_x_text;
static struct text *offset_x_var_text;
static struct text *offset_y_text;
static struct text *offset_y_var_text;

static uint8_t is_initialized;

uint8_t settings_init(const struct rect *placement,
                      const struct rect *parent_bounds)
{
    if (is_initialized || !placement || !parent_bounds) {
        return 1;
    }

    if (!background_color_text) {
        background_color_text = font_get_text_with_id(
            font_create_texture("Background Color Picker:",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                -1));
        if (!background_color_text) {
            return 1;
        }
    }

    if (!foreground_color_text) {
        foreground_color_text = font_get_text_with_id(
            font_create_texture("Foreground Color Picker:",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                -1));
        if (!foreground_color_text) {
            return 1;
        }
    }

    if (!rotation_angle_text) {
        rotation_angle_text =
            font_get_text_with_id(font_create_texture("Ângulo de Rotação:",
                                                      FONT_SIZE_MEDIUM,
                                                      BUTTON_DEFAULT_FG_COLOR,
                                                      -1));
        if (!rotation_angle_text) {
            return 1;
        }
    }

    if (!rotation_var_angle_text) {
        rotation_var_angle_text =
            font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT);
        if (!rotation_angle_text) {
            return 1;
        }
    }

    if (!scale_factor_x_text) {
        scale_factor_x_text = font_get_text_with_id(
            font_create_texture("Fator de Escala no Eixo X * 100:",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                -1));
        if (!scale_factor_x_text) {
            return 1;
        }
    }

    if (!scale_factor_x_var_text) {
        scale_factor_x_var_text =
            font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT);
        if (!scale_factor_x_var_text) {
            return 1;
        }
    }

    if (!scale_factor_y_text) {
        scale_factor_y_text = font_get_text_with_id(
            font_create_texture("Fator de Escala no Eixo Y * 100:",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                -1));
        if (!scale_factor_y_text) {
            return 1;
        }
    }

    if (!scale_factor_y_var_text) {
        scale_factor_y_var_text =
            font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT);
        if (!scale_factor_y_var_text) {
            return 1;
        }
    }

    if (!offset_x_text) {
        offset_x_text = font_get_text_with_id(
            font_create_texture("Fator de Translação no Eixo X:",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                -1));
        if (!offset_x_text) {
            return 1;
        }
    }

    if (!offset_x_var_text) {
        offset_x_var_text =
            font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT);
        if (!offset_x_var_text) {
            return 1;
        }
    }

    if (!offset_y_text) {
        offset_y_text = font_get_text_with_id(
            font_create_texture("Fator de Translação no Eixo Y:",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                -1));
        if (!offset_y_text) {
            return 1;
        }
    }

    if (!offset_y_var_text) {
        offset_y_var_text =
            font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT);
        if (!offset_y_var_text) {
            return 1;
        }
    }

    if (rect_fits(parent_bounds, placement) != 0) {
        LOG_ERROR("Settings won't fit in parent_bounds.\n");
        return 1;
    }

    memcpy((void *)&settings.placement,
           (const void *)placement,
           sizeof(struct rect));

    const int32_t offset_x = 20;
    const int32_t offset_y = 30;
    const int32_t width    = (int32_t)round(settings.placement.w * 0.45);
    const int32_t height   = 200;

    struct rect child_placement = {.x = placement->x + offset_x,
                                   .y = placement->y + offset_y,
                                   .w = width,
                                   .h = height};

    if (color_picker_init(&settings.background_picker,
                          &child_placement,
                          &settings.placement,
                          &CANVAS_DEFAULT_BG_COLOR)
        != 0) {
        return 1;
    }

    child_placement.x += width + offset_x;

    if (color_picker_init(&settings.foreground_picker,
                          &child_placement,
                          &settings.placement,
                          &CANVAS_DEFAULT_FG_COLOR)
        != 0) {
        return 1;
    }

    child_placement.x = placement->x + offset_x * 2;
    child_placement.y += height + offset_y;
    child_placement.w = (int32_t)round(settings.placement.w * 0.5);
    child_placement.h = 20;

    const struct color slider_bar_color = {.r = 0x40, .g = 0x40, .b = 0x00};

    if (slider_init(&settings.angle_slider,
                    &child_placement,
                    &settings.placement)
        != 0) {
        return 1;
    }
    slider_set_min_max(&settings.angle_slider, 0, 360);
    slider_set_value(&settings.angle_slider, 90);
    slider_set_bar_color(&settings.angle_slider, &slider_bar_color);

    child_placement.y += child_placement.h + offset_y;
    if (slider_init(&settings.scale_factor_x,
                    &child_placement,
                    &settings.placement)
        != 0) {
        return 1;
    }
    slider_set_min_max(&settings.scale_factor_x, 0, 200);
    slider_set_value(&settings.scale_factor_x, 150);
    slider_set_bar_color(&settings.scale_factor_x, &slider_bar_color);

    child_placement.y += child_placement.h + offset_y;
    if (slider_init(&settings.scale_factor_y,
                    &child_placement,
                    &settings.placement)
        != 0) {
        return 1;
    }
    slider_set_min_max(&settings.scale_factor_y, 0, 200);
    slider_set_value(&settings.scale_factor_y, 150);
    slider_set_bar_color(&settings.scale_factor_y, &slider_bar_color);

    child_placement.y += child_placement.h + offset_y;
    if (slider_init(&settings.offset_x, &child_placement, &settings.placement)
        != 0) {
        return 1;
    }
    slider_set_min_max(&settings.offset_x, 0, 200);
    slider_set_value(&settings.offset_x, 100);
    slider_set_bar_color(&settings.offset_x, &slider_bar_color);

    child_placement.y += child_placement.h + offset_y;
    if (slider_init(&settings.offset_y, &child_placement, &settings.placement)
        != 0) {
        return 1;
    }
    slider_set_min_max(&settings.offset_y, 0, 200);
    slider_set_value(&settings.offset_y, 100);
    slider_set_bar_color(&settings.offset_y, &slider_bar_color);

    is_initialized = 1;

    LOG_INFO("Creating settings with %iw %ih %ix %iy.\n",
             settings.placement.w,
             settings.placement.h,
             settings.placement.x,
             settings.placement.y);
    return 0;
}

uint8_t settings_destroy()
{
    if (!is_initialized) {
        return 1;
    }

    memset((void *)&settings, 0, sizeof(struct settings));
    return 0;
}

uint8_t settings_render()
{
    if (!is_initialized) {
        return 1;
    }

    SDL_Renderer *renderer = window_get_renderer();

    struct color bg = SETTINGS_DEFAULT_BG_COLOR;
    SDL_SetRenderDrawColor(renderer, bg.r, bg.g, bg.b, 0xFF);

    SDL_Rect rect;
    memcpy((void *)&rect, (const void *)&settings.placement, sizeof(SDL_Rect));
    SDL_RenderFillRect(renderer, &rect);

    rect.x = settings.background_picker.placement.x;
    rect.y =
        settings.background_picker.placement.y - 2 - background_color_text->h;
    rect.w = background_color_text->w;
    rect.h = background_color_text->h;
    SDL_RenderCopy(renderer, background_color_text->texture, NULL, &rect);

    rect.x = settings.foreground_picker.placement.x;
    rect.y =
        settings.foreground_picker.placement.y - 2 - foreground_color_text->h;
    rect.w = foreground_color_text->w;
    rect.h = foreground_color_text->h;
    SDL_RenderCopy(renderer, foreground_color_text->texture, NULL, &rect);

    color_picker_render(&settings.background_picker);
    color_picker_render(&settings.foreground_picker);

    rect.x = settings.angle_slider.placement.x - 20;
    rect.y = settings.angle_slider.placement.y - 2 - rotation_angle_text->h;
    rect.w = rotation_angle_text->w;
    rect.h = rotation_angle_text->h;
    SDL_RenderCopy(renderer, rotation_angle_text->texture, NULL, &rect);

    rect.x += rotation_angle_text->w + 5;
    rect.w = rotation_var_angle_text->w;
    rect.h = rotation_var_angle_text->h;
    SDL_RenderCopy(renderer, rotation_var_angle_text->texture, NULL, &rect);
    slider_render(&settings.angle_slider);

    rect.x = settings.scale_factor_x.placement.x - 20;
    rect.y = settings.scale_factor_x.placement.y - 2 - scale_factor_x_text->h;
    rect.w = scale_factor_x_text->w;
    rect.h = scale_factor_x_text->h;
    SDL_RenderCopy(renderer, scale_factor_x_text->texture, NULL, &rect);

    rect.x += scale_factor_x_text->w + 5;
    rect.w = scale_factor_x_var_text->w;
    rect.h = scale_factor_x_var_text->h;
    SDL_RenderCopy(renderer, scale_factor_x_var_text->texture, NULL, &rect);
    slider_render(&settings.scale_factor_x);

    rect.x = settings.scale_factor_y.placement.x - 20;
    rect.y = settings.scale_factor_y.placement.y - 2 - scale_factor_y_text->h;
    rect.w = scale_factor_y_text->w;
    rect.h = scale_factor_y_text->h;
    SDL_RenderCopy(renderer, scale_factor_y_text->texture, NULL, &rect);

    rect.x += scale_factor_y_text->w + 5;
    rect.w = scale_factor_y_var_text->w;
    rect.h = scale_factor_y_var_text->h;
    SDL_RenderCopy(renderer, scale_factor_y_var_text->texture, NULL, &rect);
    slider_render(&settings.scale_factor_y);

    rect.x = settings.offset_x.placement.x - 20;
    rect.y = settings.offset_x.placement.y - 2 - offset_x_text->h;
    rect.w = offset_x_text->w;
    rect.h = offset_x_text->h;
    SDL_RenderCopy(renderer, offset_x_text->texture, NULL, &rect);

    rect.x += offset_x_text->w + 5;
    rect.w = offset_x_var_text->w;
    rect.h = offset_x_var_text->h;
    SDL_RenderCopy(renderer, offset_x_var_text->texture, NULL, &rect);
    slider_render(&settings.offset_x);

    rect.x = settings.offset_y.placement.x - 20;
    rect.y = settings.offset_y.placement.y - 2 - offset_y_text->h;
    rect.w = offset_y_text->w;
    rect.h = offset_y_text->h;
    SDL_RenderCopy(renderer, offset_y_text->texture, NULL, &rect);

    rect.x += offset_y_text->w + 5;
    rect.w = offset_y_var_text->w;
    rect.h = offset_y_var_text->h;
    SDL_RenderCopy(renderer, offset_y_var_text->texture, NULL, &rect);
    slider_render(&settings.offset_y);

    return 0;
}

uint8_t settings_handle_mouse(uint32_t state, int32_t x, int32_t y)
{
    static int32_t cached_angle_value   = 0;
    static int32_t cached_scale_x_value = 0;
    static int32_t cached_scale_y_value = 0;
    static int32_t cached_offset_x      = 0;
    static int32_t cached_offset_y      = 0;

    if (!is_initialized) {
        return 1;
    }

    if (rect_contains_point(&settings.placement, x, y) != 0) {
        return 1;
    }

    color_picker_handle_mouse(&settings.background_picker, state, x, y);
    color_picker_handle_mouse(&settings.foreground_picker, state, x, y);
    slider_handle_mouse(&settings.angle_slider, state, x, y);
    slider_handle_mouse(&settings.scale_factor_x, state, x, y);
    slider_handle_mouse(&settings.scale_factor_y, state, x, y);
    slider_handle_mouse(&settings.offset_x, state, x, y);
    slider_handle_mouse(&settings.offset_y, state, x, y);

    if (settings.angle_slider.value != cached_angle_value) {
        rotation_var_angle_text = font_get_text_with_id(
            NEGATIVE_NUMERAL_TEXTURE_COUNT + settings.angle_slider.value);
        cached_angle_value = settings.angle_slider.value;
    }

    if (settings.scale_factor_x.value != cached_scale_x_value) {
        scale_factor_x_var_text = font_get_text_with_id(
            NEGATIVE_NUMERAL_TEXTURE_COUNT + settings.scale_factor_x.value);
        cached_scale_x_value = settings.scale_factor_x.value;
    }

    if (settings.scale_factor_y.value != cached_scale_y_value) {
        scale_factor_y_var_text = font_get_text_with_id(
            NEGATIVE_NUMERAL_TEXTURE_COUNT + settings.scale_factor_y.value);
        cached_scale_y_value = settings.scale_factor_y.value;
    }

    if (settings.offset_x.value != cached_offset_x) {
        offset_x_var_text = font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT
                                                  + settings.offset_x.value);
        cached_offset_x   = settings.offset_x.value;
    }

    if (settings.offset_y.value != cached_offset_y) {
        offset_y_var_text = font_get_text_with_id(NEGATIVE_NUMERAL_TEXTURE_COUNT
                                                  + settings.offset_y.value);
        cached_offset_y   = settings.offset_y.value;
    }

    return 0;
}

const struct color *settings_get_background_color()
{
    if (!is_initialized) {
        return NULL;
    }

    return &settings.background_picker.color;
}

const struct color *settings_get_foreground_color()
{
    if (!is_initialized) {
        return NULL;
    }

    return &settings.foreground_picker.color;
}

int32_t settings_get_rotation_angle()
{
    if (!is_initialized) {
        return -1;
    }

    return settings.angle_slider.value;
}

double settings_get_scale_factor_x()
{
    if (!is_initialized) {
        return -1.0;
    }

    return (double)settings.scale_factor_x.value / 100.0;
}

double settings_get_scale_factor_y()
{
    if (!is_initialized) {
        return -1.0;
    }

    return (double)settings.scale_factor_y.value / 100.0;
}

int32_t settings_get_offset_x()
{
    if (!is_initialized) {
        return -1;
    }

    return settings.offset_x.value;
}

int32_t settings_get_offset_y()
{
    if (!is_initialized) {
        return -1;
    }

    return settings.offset_y.value;
}
