/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "font.h"

#include "log.h"
#include "window.h"

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>

#define DEFAULT_FONT "font/OpenSans-Regular.ttf"

#define SMALL_FONT_SIZE  ((uint8_t)11)
#define MEDIUM_FONT_SIZE ((uint8_t)16)
#define BIG_FONT_SIZE    ((uint8_t)22)

struct text_entry {
    int32_t id;
    struct text text;
    char *str;

    struct text_entry *next;
};

static TTF_Font *small_font;
static TTF_Font *medium_font;
static TTF_Font *big_font;

static struct text_entry *text_list_root;
static uint8_t is_initialized;

uint8_t font_init()
{
    if (is_initialized) {
        return 1;
    }

    if (TTF_Init() < 0) {
        LOG_ERROR("SDL_ttf initialization failed: %s.\n", TTF_GetError());
        return 1;
    }

    small_font  = TTF_OpenFont(DEFAULT_FONT, SMALL_FONT_SIZE);
    medium_font = TTF_OpenFont(DEFAULT_FONT, MEDIUM_FONT_SIZE);
    big_font    = TTF_OpenFont(DEFAULT_FONT, BIG_FONT_SIZE);

    if (!small_font || !medium_font || !big_font) {
        LOG_ERROR("SDL_ttf could not load font: %s.\n", TTF_GetError());
        return 1;
    }

    return 0;
}

uint8_t font_destroy()
{
    struct text_entry *entry = text_list_root;
    while (entry) {
        struct text_entry *tmp = entry->next;
        SDL_DestroyTexture(entry->text.texture);
        free(entry->str);
        free(entry);
        entry = tmp;
    }

    text_list_root = NULL;

    TTF_CloseFont(small_font);
    TTF_CloseFont(medium_font);
    TTF_CloseFont(big_font);
    TTF_Quit();

    is_initialized = 1;
    return 0;
}

int32_t font_create_texture(const char *text,
                            enum FONT_SIZE size,
                            struct color color,
                            int32_t forced_id)
{
    if (!text || size < FONT_SIZE_SMALL || size > FONT_SIZE_ENUM_SIZE) {
        return -1;
    }

    TTF_Font *font;
    if (size == FONT_SIZE_SMALL) {
        font = small_font;
    } else if (size == FONT_SIZE_MEDIUM) {
        font = medium_font;
    } else if (size == FONT_SIZE_BIG) {
        font = big_font;
    } else {
        return -1;
    }

    SDL_Color sdl_color       = {color.r, color.g, color.b, 0xFF};
    SDL_Surface *text_surface = TTF_RenderUTF8_Blended(font, text, sdl_color);
    if (!text_surface) {
        LOG_ERROR("Couldn't create surface for text.\n");
        return -1;
    }

    SDL_Texture *text_texture =
        SDL_CreateTextureFromSurface(window_get_renderer(), text_surface);

    if (!text_texture) {
        LOG_ERROR("Couldnt create texture for text.\n");
        SDL_FreeSurface(text_surface);
        return -1;
    }

    struct text_entry *new_entry = NULL;

    // Não é a primeira entrada da  lista.
    if (text_list_root) {
        struct text_entry *entry = text_list_root;
        while (entry->next) {
            entry = entry->next;
        }

        new_entry = malloc(sizeof(struct text_entry));
        if (!new_entry) {
            LOG_ERROR("Couldn't create new_entry.\n");
            SDL_FreeSurface(text_surface);
            return -1;
        }

        new_entry->id           = (forced_id < 0) ? entry->id + 1 : forced_id;
        new_entry->text.texture = text_texture;
        new_entry->text.w       = text_surface->w;
        new_entry->text.h       = text_surface->h;
        new_entry->str          = strdup(text);

        if (!new_entry->str) {
            LOG_ERROR("Couldn't create new_entry.\n");
            SDL_FreeSurface(text_surface);
            free(new_entry);
            return -1;
        }

        new_entry->next = NULL;

        entry->next = new_entry;
    } else {
        text_list_root = malloc(sizeof(struct text_entry));
        if (text_list_root) {
            text_list_root->id           = (forced_id < 0) ? 0 : forced_id;
            text_list_root->text.texture = text_texture;
            text_list_root->text.w       = text_surface->w;
            text_list_root->text.h       = text_surface->h;
            text_list_root->str          = strdup(text);

            if (!text_list_root->str) {
                LOG_ERROR("Couldn't create new_entry.\n");
                SDL_FreeSurface(text_surface);
                free(text_list_root);
                return -1;
            }

            text_list_root->next = NULL;

            new_entry = text_list_root;
        } else {
            SDL_FreeSurface(text_surface);
            return -1;
        }
    }

    SDL_FreeSurface(text_surface);

    return new_entry->id;
}

struct text *font_get_text_with_str(const char *str)
{
    if (!str) {
        return NULL;
    }

    // Checa se esse texto existe.
    for (struct text_entry *entry = text_list_root; entry;
         entry                    = entry->next) {
        if (strcmp(entry->str, str) == 0) {
            return &entry->text;
        }
    }

    return NULL;
}

struct text *font_get_text_with_id(int32_t id)
{
    if (id < 0) {
        return NULL;
    }

    // Checa se algum texto com esse ID já existe.
    for (struct text_entry *entry = text_list_root; entry;
         entry                    = entry->next) {
        if (entry->id == id) {
            return &entry->text;
        }
    }

    return NULL;
}
