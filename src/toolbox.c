/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "toolbox.h"

#include "button.h"
#include "color.h"
#include "font.h"
#include "log.h"
#include "rect.h"
#include "window.h"

#include <stdlib.h>
#include <string.h>

static struct toolbox toolbox;
static uint8_t is_initialized;

uint8_t toolbox_init(const struct rect *placement,
                     const struct rect *parent_bounds)
{
    if (is_initialized || !placement || !parent_bounds) {
        return 1;
    }

    if (rect_fits(parent_bounds, placement) != 0) {
        LOG_ERROR("Toolbox won't fit parent_bounds.\n");
        return 1;
    }

    memcpy((void *)&toolbox.placement,
           (const void *)placement,
           sizeof(struct rect));
    is_initialized = 1;

    if (toolbox_set_current_mode_text(font_get_text_with_id(
            font_create_texture("Nada selecionado.",
                                FONT_SIZE_MEDIUM,
                                BUTTON_DEFAULT_FG_COLOR,
                                NOTHING_SELECTED_TEXT_ID)))
        != 0) {
        is_initialized = 0;
        return 1;
    }

    toolbox.button_list_root = NULL;

    LOG_INFO("Created toolbox with %iw %ih %ix %iy.\n",
             toolbox.placement.w,
             toolbox.placement.h,
             toolbox.placement.x,
             toolbox.placement.y);
    return 0;
}

uint8_t toolbox_destroy()
{
    if (!is_initialized) {
        return 1;
    }

    struct button_entry *entry = toolbox.button_list_root;
    while (entry) {
        struct button_entry *tmp = entry->next;
        free(entry);
        entry = tmp;
    }

    memset((void *)&toolbox, 0, sizeof(struct toolbox));
    return 0;
}

uint8_t toolbox_render()
{
    if (!is_initialized) {
        return 1;
    }

    SDL_Renderer *renderer = window_get_renderer();

    // Desenha a toolbox.
    SDL_SetRenderDrawColor(renderer,
                           TOOLBOX_DEFAULT_BG_COLOR.r,
                           TOOLBOX_DEFAULT_BG_COLOR.g,
                           TOOLBOX_DEFAULT_BG_COLOR.b,
                           0xFF);
    SDL_Rect *toolbox_rect = (SDL_Rect *)&toolbox.placement;
    SDL_RenderFillRect(renderer, toolbox_rect);

    // Passa por cada botão.
    for (struct button_entry *entry = toolbox.button_list_root; entry;
         entry                      = entry->next) {
        button_render(entry->button);
    }

    // Renderiza texto, se houver algum.
    if (toolbox.current_mode_text) {
        SDL_Rect *dest = (SDL_Rect *)&toolbox.current_mode_text_placement;
        SDL_RenderCopy(renderer,
                       toolbox.current_mode_text->texture,
                       NULL,
                       dest);
    }

    return 0;
}

uint8_t toolbox_handle_mouse(uint32_t state, int32_t x, int32_t y)
{
    if (!is_initialized) {
        return 1;
    }

    if (rect_contains_point(&toolbox.placement, x, y) != 0) {
        return 1;
    }

    for (struct button_entry *entry = toolbox.button_list_root; entry;
         entry                      = entry->next) {
        button_handle_mouse(entry->button, state, x, y);
    }

    return 0;
}

uint8_t toolbox_attach_button(struct button *button)
{
    if (!is_initialized || !button || !button->is_initialized) {
        return 1;
    }

    // Não é a primeira entrada da lista.
    if (toolbox.button_list_root) {
        struct button_entry *entry = toolbox.button_list_root;
        while (entry->next) {
            entry = entry->next;
        }

        // entry->next é nulo.
        struct button_entry *new_entry = malloc(sizeof(struct button_entry));
        if (!new_entry) {
            LOG_ERROR("Couldn't create new_entry.\n");
            return 1;
        }

        new_entry->button = button;
        new_entry->next   = NULL;

        entry->next = new_entry;
    } else {
        // Primeira entrada da lista.
        toolbox.button_list_root = malloc(sizeof(struct button_entry));
        if (toolbox.button_list_root) {
            toolbox.button_list_root->button = button;
            toolbox.button_list_root->next   = NULL;
        } else {
            LOG_ERROR("Couldn't create first button list entry.\n");
            return 1;
        }
    }

    return 0;
}

uint8_t toolbox_set_current_mode_text(const struct text *text)
{
    if (!is_initialized || !text) {
        LOG_INFO("Not able to set current mode text.\n");
        return 1;
    }

    toolbox.current_mode_text = text;

    toolbox.current_mode_text_placement.w = text->w;
    toolbox.current_mode_text_placement.h = text->h;
    toolbox.current_mode_text_placement.x =
        toolbox.placement.x + (toolbox.placement.w / 2) - (text->w / 2);
    toolbox.current_mode_text_placement.y = toolbox.placement.h - text->h;

    return 0;
}

const struct rect *toolbox_get_placement()
{
    return &toolbox.placement;
}
