/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "transforms.h"

#include "canvas.h"
#include "list.h"
#include "log.h"
#include "min_max.h"
#include "rect.h"
#include "settings.h"

#include <math.h>

/**
 * @brief Rotaciona um ponto.
 *
 * @param x
 * @param y
 * @param center_x
 * @param center_y
 * @param cosine
 * @param sine
 */
static void rotate_point(int32_t *x,
                         int32_t *y,
                         int32_t center_x,
                         int32_t center_y,
                         double cosine,
                         double sine);

/**
 * @brief
 *
 * @param x
 * @param y
 * @param canvas_placement
 * @param axis
 */
static void reflect_point(int32_t *x,
                          int32_t *y,
                          const struct rect *canvas_placement,
                          uint8_t axis);

void transforms_apply_translation(struct list *lines,
                                  struct list *circs,
                                  int32_t off_x,
                                  int32_t off_y)
{
    if (!lines || !circs) {
        return;
    }

    for (uint64_t u = 0; u != lines->size; ++u) {
        struct line *entry = &LIST_AT(lines, u, struct line);

        entry->start_x += off_x;
        entry->start_y += off_y;

        entry->end_x += off_x;
        entry->end_y += off_y;
    }

    for (uint64_t u = 0; u != circs->size; ++u) {
        struct circumference *entry = &LIST_AT(circs, u, struct circumference);

        entry->center_x += off_x;
        entry->center_y += off_y;
    }
}

void transforms_apply_rotation(struct list *lines, struct list *circs)
{
    const struct rect *canvas_placement = canvas_get_placement();

    if (!lines) {
        return;
    }

    const double radians = settings_get_rotation_angle() * M_PI / 180.0;
    const double cosine  = cos(radians);
    const double sine    = sin(radians);

    // Rotação acontecerá em relação ao centro do canvas.
    const int32_t center_x = canvas_placement->w / 2;
    const int32_t center_y = canvas_placement->h / 2;

    for (uint64_t u = 0; u != lines->size; ++u) {
        struct line *entry = &LIST_AT(lines, u, struct line);

        rotate_point(&entry->start_x,
                     &entry->start_y,
                     center_x,
                     center_y,
                     cosine,
                     sine);
        rotate_point(&entry->end_x,
                     &entry->end_y,
                     center_x,
                     center_y,
                     cosine,
                     sine);
    }

    for (uint64_t u = 0; u != circs->size; ++u) {
        struct circumference *entry = &LIST_AT(circs, u, struct circumference);

        rotate_point(&entry->center_x,
                     &entry->center_y,
                     center_x,
                     center_y,
                     cosine,
                     sine);
    }
}

void transforms_apply_scaling(struct list *lines, struct list *circs)
{
    const double factor_x = settings_get_scale_factor_x();
    const double factor_y = settings_get_scale_factor_y();

    for (uint64_t u = 0; u != lines->size; ++u) {
        struct line *entry = &LIST_AT(lines, u, struct line);

        entry->start_x = round((double)entry->start_x * factor_x);
        entry->start_y = round((double)entry->start_y * factor_y);

        entry->end_x = round((double)entry->end_x * factor_x);
        entry->end_y = round((double)entry->end_y * factor_y);
    }

    for (uint64_t u = 0; u != circs->size; ++u) {
        struct circumference *entry = &LIST_AT(circs, u, struct circumference);

        entry->radius   = round((double)entry->radius * factor_x);
        entry->center_x = round((double)entry->center_x * factor_x);
        entry->center_y = round((double)entry->center_y * factor_y);
    }
}

void transforms_apply_reflection(struct list *lines,
                                 struct list *circs,
                                 uint8_t axis)
{
    const struct rect *canvas_placement = canvas_get_placement();

    for (uint64_t u = 0; u != lines->size; ++u) {
        struct line *entry = &LIST_AT(lines, u, struct line);

        reflect_point(&entry->start_x, &entry->start_y, canvas_placement, axis);
        reflect_point(&entry->end_x, &entry->end_y, canvas_placement, axis);
    }

    for (uint64_t u = 0; u != circs->size; ++u) {
        struct circumference *entry = &LIST_AT(circs, u, struct circumference);

        reflect_point(&entry->center_x,
                      &entry->center_y,
                      canvas_placement,
                      axis);
    }
}

static void reflect_point(int32_t *x,
                          int32_t *y,
                          const struct rect *canvas_placement,
                          uint8_t axis)
{
    const double center_x = (double)canvas_placement->w / 2.0;
    const double center_y = (double)canvas_placement->h / 2.0;

    if ((axis & Y_AXIS_REFLECTION_BIT) != 0) {
        if (*x < center_x) {
            *x = canvas_placement->w - *x;
        } else if (*x > center_x) {
            *x = abs(*x - canvas_placement->w);
        }
    }

    if ((axis & X_AXIS_REFLECTION_BIT) != 0) {
        if (*y < center_y) {
            *y = canvas_placement->h - *y;
        } else if (*y > center_y) {
            *y = abs(*y - canvas_placement->h);
        }
    }
}

static void rotate_point(int32_t *x,
                         int32_t *y,
                         int32_t center_x,
                         int32_t center_y,
                         double cosine,
                         double sine)
{
    const int32_t adjusted_x = *x - center_x;
    const int32_t adjusted_y = *y - center_y;

    *x = round(center_x + (cosine * adjusted_x) + (sine * adjusted_y));
    *y = round(center_y + (-sine * adjusted_x) + (cosine * adjusted_y));
}
