/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "line_clipping.h"

#include "canvas.h"
#include "list.h"
#include "log.h"
#include "rect.h"

#include <stdint.h>

#define LEFT_BIT   ((uint8_t)0x01)
#define RIGHT_BIT  ((uint8_t)0x02)
#define TOP_BIT    ((uint8_t)0x04)
#define BOTTOM_BIT ((uint8_t)0x08)

/**
 * @brief Gera um código que será usado para identificar a posição
 * relativa de um ponto a viewport para uso no algoritmo de cohen sutherland.
 *
 * @param clip_rect Clip Rect ou Viewport.
 * @param x Posição X do ponto.
 * @param y Posição Y do ponto.
 * @return uint8_t Código.
 */
static uint8_t generate_cs_code(const struct rect *clip_rect,
                                int32_t x,
                                int32_t y);

/**
 * @brief Retorna o menor valor no arranjo.
 *
 * @param size Tamanho do arranjo.
 * @param arr Arranjo de doubles.
 * @return double Menor valor.
 */
static double minimum(uint32_t size, const double *arr);

/**
 * @brief Retorna o maior valor no arranjo.
 *
 * @param size Tamanho do arranjo.
 * @param arr Arranjo de doubles.
 * @return double Maior valor.
 */
static double maximum(uint32_t size, const double *arr);

void line_clipping_cohen_sutherland(const struct rect *clip_rect,
                                    struct list *lines)
{
    const int32_t x_max = clip_rect->x + clip_rect->w;
    const int32_t y_max = clip_rect->y + clip_rect->h;
    const int32_t x_min = clip_rect->x;
    const int32_t y_min = clip_rect->y;

    for (uint64_t u = 0; u != lines->size; ++u) {
        struct line *line = &LIST_AT(lines, u, struct line);

        int32_t start_x = line->start_x;
        int32_t start_y = line->start_y;
        int32_t end_x   = line->end_x;
        int32_t end_y   = line->end_y;

        uint8_t p1_code = generate_cs_code(clip_rect, start_x, start_y);
        uint8_t p2_code = generate_cs_code(clip_rect, end_x, end_y);

        uint8_t clipped = 0;
        while (1) {
            if (p1_code == 0 && p2_code == 0) {
                // Os dois pontos estão dentro da viewport.
                LOG_INFO("Both points are inside the viewport.\n");
                clipped = 1;
                break;
            } else if ((p1_code & p2_code) != 0) {
                LOG_INFO("Both points are outside the viewport.\n");
                // Nesse caso, as regiões dos dois pontos são iguais. Isso faz
                // com que estejam fora da viewport.
                break;
            } else {
                // Nesse caso, temos testes para fazer!
                const uint8_t out_point_code =
                    p2_code > p1_code ? p2_code : p1_code;

                int32_t nx = 0;
                int32_t ny = 0;

                if ((out_point_code & TOP_BIT) != 0) {
                    nx = start_x
                         + (end_x - start_x) * (y_min - start_y)
                               / (end_y - start_y);
                    ny = y_min;
                } else if ((out_point_code & BOTTOM_BIT) != 0) {
                    nx = start_x
                         + (end_x - start_x) * (y_max - start_y)
                               / (end_y - start_y);
                    ny = y_max;
                } else if ((out_point_code & LEFT_BIT) != 0) {
                    nx = x_min;
                    ny = start_y
                         + (end_y - start_y) * (x_min - start_x)
                               / (end_x - start_x);
                } else if ((out_point_code & RIGHT_BIT) != 0) {
                    nx = x_max;
                    ny = start_y
                         + (end_y - start_y) * (x_max - start_x)
                               / (end_x - start_x);
                }

                if (p1_code == out_point_code) {
                    start_x = nx;
                    start_y = ny;
                    p1_code = generate_cs_code(clip_rect, start_x, start_y);
                } else {
                    end_x   = nx;
                    end_y   = ny;
                    p2_code = generate_cs_code(clip_rect, end_x, end_y);
                }
            }
        }

        if (clipped) {
            line->start_x = start_x;
            line->start_y = start_y;
            line->end_x   = end_x;
            line->end_y   = end_y;
        }
    }
}

void line_clipping_liang_barsky(const struct rect *clip_rect,
                                struct list *lines)
{
    const int32_t x_max = clip_rect->x + clip_rect->w;
    const int32_t y_max = clip_rect->y + clip_rect->h;
    const int32_t x_min = clip_rect->x;
    const int32_t y_min = clip_rect->y;

    for (uint64_t u = 0; u != lines->size; ++u) {
        struct line *line = &LIST_AT(lines, u, struct line);

        // As linhas criadas no canvas sempre tem a menor coordenada em start
        // e a maior em end.
        const double p1 = -(line->end_x - line->start_x);
        const double p2 = -p1;
        const double p3 = -(line->end_y - line->start_y);
        const double p4 = -p3;

        const double q1 = line->start_x - x_min;
        const double q2 = x_max - line->start_x;
        const double q3 = line->start_y - y_min;
        const double q4 = y_max - line->start_y;

        double positive_array[5];
        double negative_array[5];

        memset((void *)positive_array, 0, sizeof(positive_array));
        memset((void *)negative_array, 0, sizeof(negative_array));

        uint32_t positive_i = 1;
        uint32_t negative_i = 1;

        positive_array[0] = 1.0;
        negative_array[0] = 0.0;

        if ((p1 == 0.0 && q1 < 0.0) || (p2 == 0.0 && q2 < 0.0)
            || (p3 == 0.0 && q3 < 0.0) || (p4 == 0.0 && q4 < 0.0)) {
            LOG_INFO("Parallel to the clipping window.\n");
            continue;
        }

        if (p1 != 0.0) {
            const double r1 = q1 / p1;
            const double r2 = q2 / p2;
            if (p1 < 0.0) {
                negative_array[negative_i++] = r1;
                positive_array[positive_i++] = r2;
            } else {
                negative_array[negative_i++] = r2;
                positive_array[positive_i++] = r1;
            }
        }

        if (p3 != 0.0) {
            const double r3 = q3 / p3;
            const double r4 = q4 / p4;
            if (p3 < 0.0) {
                negative_array[negative_i++] = r3;
                positive_array[positive_i++] = r4;
            } else {
                negative_array[negative_i++] = r4;
                positive_array[positive_i++] = r3;
            }
        }

        const double negative = maximum(negative_i, negative_array);
        const double positive = minimum(positive_i, positive_array);

        if (negative > positive) {
            LOG_INFO("Outside the clipping window.\n");
            continue;
        }

        const int32_t n_start_x = round(line->start_x + p2 * negative);
        const int32_t n_start_y = round(line->start_y + p4 * negative);

        const int32_t n_end_x = round(line->start_x + p2 * positive);
        const int32_t n_end_y = round(line->start_y + p4 * positive);

        line->start_x = n_start_x;
        line->start_y = n_start_y;

        line->end_x = n_end_x;
        line->end_y = n_end_y;
    }
}

static uint8_t generate_cs_code(const struct rect *clip_rect,
                                int32_t x,
                                int32_t y)
{
    uint8_t code = 0;

    if (x < clip_rect->x) {
        code |= LEFT_BIT;
    } else if (x > clip_rect->x + clip_rect->w) {
        code |= RIGHT_BIT;
    }

    if (y < clip_rect->y) {
        code |= TOP_BIT;
    } else if (y > clip_rect->y + clip_rect->h) {
        code |= BOTTOM_BIT;
    }

    return code;
}

static double minimum(uint32_t size, const double *arr)
{
    double x = arr[0];
    for (uint32_t u = 1; u != size; ++u) {
        if (arr[u] < x) {
            x = arr[u];
        }
    }

    return x;
}

static double maximum(uint32_t size, const double *arr)
{
    double x = arr[0];
    for (uint32_t u = 1; u != size; ++u) {
        if (arr[u] > x) {
            x = arr[u];
        }
    }

    return x;
}
