/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "canvas.h"

#include "font.h"
#include "line_clipping.h"
#include "list.h"
#include "log.h"
#include "min_max.h"
#include "rasterizer.h"
#include "rect.h"
#include "settings.h"
#include "toolbox.h"
#include "transforms.h"
#include "window.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>

#define DEBOUNCE_MS ((uint32_t)200)

#define CANVAS_NEEDS_RECT_BIT ((uint8_t)0x01)
#define CANVAS_NEEDS_AXIS_BIT ((uint8_t)0x02)

#define LINE_DDA_BIT       ((uint8_t)0x01)
#define LINE_BRESENHAM_BIT ((uint8_t)0x02)

enum CANVAS_STATES
{
    CANVAS_STATE_NONE = 0,
    CANVAS_STATE_DDA_LINE,
    CANVAS_STATE_BRESENHAM_LINE,
    CANVAS_STATE_BRESENHAM_CIRCUMFERENCE,
    CANVAS_STATE_COHEN_SUTHERLAND,
    CANVAS_STATE_LIANG_BARSKY
};

enum LINE_TOOL_STATES
{
    LINE_TOOL_STATE_WAITING_POINT_1 = 0,
    LINE_TOOL_STATE_DEBOUNCE,
    LINE_TOOL_STATE_WAITING_POINT_2,
};

enum CIRC_TOOL_STATES
{
    CIRC_TOOL_STATE_WAITING_CENTER = 0,
    CIRC_TOOL_STATE_DEBOUNCE,
    CIRC_TOOL_STATE_WAITING_RADIUS,
};

enum LINE_CLIPPING_TOOL_STATES
{
    LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_1 = 0,
    LINE_CLIPPING_TOOL_STATE_DEBOUNCE_3,
    LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_2
};

struct line_tool {
    struct line line;
    uint32_t debounce_timer;

    enum LINE_TOOL_STATES state;
};

struct circ_tool {
    struct circumference circ;
    uint32_t last_mouse_x;
    uint32_t last_mouse_y;
    uint32_t debounce_timer;

    enum CIRC_TOOL_STATES state;
};

struct line_clipping_tool {
    struct rect clip_rect;
    uint32_t debounce_timer;

    enum LINE_CLIPPING_TOOL_STATES state;
};

struct canvas {
    struct rect placement;

    struct color background_color;
    struct color foreground_color;

    enum CANVAS_STATES state;
    union {
        struct line_tool line_tool;
        struct circ_tool circ_tool;
        struct line_clipping_tool line_clipping_tool;
    } tools;
    uint8_t state_options;
};

/**
 * @brief Máquina de estado para rasterização de linhas.
 *
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 *
 * @return uint8_t
 */
static uint8_t canvas_line_state_machine(uint32_t state, int32_t x, int32_t y);

/**
 * @brief Máquina de estado para rasterização de circunferência.
 *
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 *
 * @return uint8_t
 */
static uint8_t canvas_circumference_state_machine(uint32_t state,
                                                  int32_t x,
                                                  int32_t y);

/**
 * @brief Máquina de estado para line clipping.
 *
 * @param state Estado do mouse, obtido por SDL_GetMouseState().
 * @param x Eixo X, obtido por SDL_GetMouseState().
 * @param y Eixo Y, obtido por SDL_GetMouseState().
 *
 * @return uint8_t
 */
static uint8_t canvas_line_clipping_state_machine(uint32_t state,
                                                  int32_t x,
                                                  int32_t y);

static struct canvas canvas;
static struct list line_list;
static struct list circ_list;
static uint8_t is_initialized;

uint8_t canvas_init(const struct rect *placement,
                    const struct rect *parent_bounds)
{
    if (is_initialized || !placement || !parent_bounds) {
        return 1;
    }

    if (list_init(&line_list, sizeof(struct line)) != 0) {
        LOG_ERROR("Failed to initialize list.\n");
        return 1;
    }

    if (list_init(&circ_list, sizeof(struct circumference)) != 0) {
        LOG_ERROR("Failed to initialize list.\n");
        return 1;
    }

    if (rect_fits(parent_bounds, placement) != 0) {
        LOG_ERROR("Canvas won't fit in parent_bounds.\n");
        return 1;
    }

    memcpy((void *)&canvas.placement,
           (const void *)placement,
           sizeof(struct rect));

    memcpy((void *)&canvas.background_color,
           (const void *)&CANVAS_DEFAULT_BG_COLOR,
           sizeof(struct color));
    memcpy((void *)&canvas.foreground_color,
           (const void *)&CANVAS_DEFAULT_FG_COLOR,
           sizeof(struct color));

    canvas.state         = CANVAS_STATE_NONE;
    canvas.state_options = 0;
    is_initialized       = 1;

    LOG_INFO("Created canvas with %iw %ih %ix %iy.\n",
             canvas.placement.w,
             canvas.placement.h,
             canvas.placement.x,
             canvas.placement.y);
    return 0;
}

uint8_t canvas_render()
{
    if (!is_initialized) {
        return 1;
    }

    SDL_Renderer *renderer = window_get_renderer();

    // Renderiza o canvas (um grande retângulo).
    struct color bg = canvas.background_color;
    SDL_SetRenderDrawColor(renderer, bg.r, bg.g, bg.b, 0xFF);

    SDL_Rect *canvas_rect = (SDL_Rect *)&canvas.placement;
    SDL_RenderFillRect(renderer, canvas_rect);

    struct color fg = canvas.foreground_color;
    SDL_SetRenderDrawColor(renderer, fg.r, fg.g, fg.b, 0xFF);

#if defined(DEBUG)
    const uint32_t raster_start = SDL_GetTicks();
#endif

    // Faz rasterização das linhas.
    for (uint64_t u = 0; u != line_list.size; ++u) {
        const struct line *entry = &LIST_AT(&line_list, u, struct line);

        if ((entry->type & LINE_DDA_BIT) != 0) {
            rasterize_dda_line(entry->start_x,
                               entry->start_y,
                               entry->end_x,
                               entry->end_y);
        } else if ((entry->type & LINE_BRESENHAM_BIT) != 0) {
            rasterize_bresenham_line(entry->start_x,
                                     entry->start_y,
                                     entry->end_x,
                                     entry->end_y);
        }
    }

    // Faz rasterização dos círculos.
    for (uint64_t u = 0; u != circ_list.size; ++u) {
        const struct circumference *entry =
            &LIST_AT(&circ_list, u, struct circumference);

        rasterize_bresenham_circumference(entry->center_x,
                                          entry->center_y,
                                          entry->radius);
    }

    // Preview da reta...
    if ((canvas.state == CANVAS_STATE_DDA_LINE
         || canvas.state == CANVAS_STATE_BRESENHAM_LINE)
        && canvas.tools.line_tool.state != LINE_TOOL_STATE_WAITING_POINT_1) {
        if (canvas.state == CANVAS_STATE_DDA_LINE) {
            rasterize_dda_line(canvas.tools.line_tool.line.start_x,
                               canvas.tools.line_tool.line.start_y,
                               canvas.tools.line_tool.line.end_x,
                               canvas.tools.line_tool.line.end_y);
        } else {
            rasterize_bresenham_line(canvas.tools.line_tool.line.start_x,
                                     canvas.tools.line_tool.line.start_y,
                                     canvas.tools.line_tool.line.end_x,
                                     canvas.tools.line_tool.line.end_y);
        }
    }

    // Preview da circunferência...
    if (canvas.state == CANVAS_STATE_BRESENHAM_CIRCUMFERENCE
        && canvas.tools.circ_tool.state != CIRC_TOOL_STATE_WAITING_CENTER) {
        rasterize_bresenham_circumference(canvas.tools.circ_tool.circ.center_x,
                                          canvas.tools.circ_tool.circ.center_y,
                                          canvas.tools.circ_tool.circ.radius);

        rasterize_dda_line(canvas.tools.circ_tool.circ.center_x,
                           canvas.tools.circ_tool.circ.center_y,
                           canvas.tools.circ_tool.last_mouse_x,
                           canvas.tools.circ_tool.last_mouse_y);
    }

    // Desenhamos o retângulo de seleção.
    if ((canvas.state_options & CANVAS_NEEDS_RECT_BIT) != 0) {
        if (canvas.state == CANVAS_STATE_COHEN_SUTHERLAND
            || canvas.state == CANVAS_STATE_LIANG_BARSKY) {
            // Esses dois estados também usam retângulo de seleção, mas são de
            // uma ferramenta diferente.
            if (canvas.tools.line_clipping_tool.state
                >= LINE_CLIPPING_TOOL_STATE_DEBOUNCE_3) {
                SDL_SetRenderDrawColor(renderer,
                                       CANVAS_DEFAULT_RECT_COLOR.r,
                                       CANVAS_DEFAULT_RECT_COLOR.g,
                                       CANVAS_DEFAULT_RECT_COLOR.b,
                                       0xFF);
                // Rasteriza as quatro linhas para fazer o retângulo de seleção.
                rasterize_bresenham_line(
                    canvas.tools.line_clipping_tool.clip_rect.x,
                    canvas.tools.line_clipping_tool.clip_rect.y,
                    canvas.tools.line_clipping_tool.clip_rect.x
                        + canvas.tools.line_clipping_tool.clip_rect.w,
                    canvas.tools.line_clipping_tool.clip_rect.y);
                rasterize_bresenham_line(
                    canvas.tools.line_clipping_tool.clip_rect.x,
                    canvas.tools.line_clipping_tool.clip_rect.y,
                    canvas.tools.line_clipping_tool.clip_rect.x,
                    canvas.tools.line_clipping_tool.clip_rect.y
                        + canvas.tools.line_clipping_tool.clip_rect.h);
                rasterize_bresenham_line(
                    canvas.tools.line_clipping_tool.clip_rect.x
                        + canvas.tools.line_clipping_tool.clip_rect.w,
                    canvas.tools.line_clipping_tool.clip_rect.y,
                    canvas.tools.line_clipping_tool.clip_rect.x
                        + canvas.tools.line_clipping_tool.clip_rect.w,
                    canvas.tools.line_clipping_tool.clip_rect.y
                        + canvas.tools.line_clipping_tool.clip_rect.h);
                rasterize_bresenham_line(
                    canvas.tools.line_clipping_tool.clip_rect.x,
                    canvas.tools.line_clipping_tool.clip_rect.y
                        + canvas.tools.line_clipping_tool.clip_rect.h,
                    canvas.tools.line_clipping_tool.clip_rect.x
                        + canvas.tools.line_clipping_tool.clip_rect.w,
                    canvas.tools.line_clipping_tool.clip_rect.y
                        + canvas.tools.line_clipping_tool.clip_rect.h);
            }
        }
    }

    // Desenhamos os eixos no canvas.
    if ((canvas.state_options & CANVAS_NEEDS_AXIS_BIT) != 0) {
        const int32_t middle_y = canvas.placement.h / 2;
        const int32_t middle_x = canvas.placement.w / 2;

        SDL_SetRenderDrawColor(renderer,
                               CANVAS_DEFAULT_AXIS_COLOR.r,
                               CANVAS_DEFAULT_AXIS_COLOR.g,
                               CANVAS_DEFAULT_AXIS_COLOR.b,
                               0xFF);
        // Eixo x.
        rasterize_bresenham_line(0, middle_y, canvas.placement.w, middle_y);

        // Eixo y.
        rasterize_bresenham_line(middle_x, 0, middle_x, canvas.placement.h);
    }

#if defined(DEBUG)
    const uint32_t raster_end = SDL_GetTicks();
    const uint32_t delta      = raster_end - raster_start;
    if (delta > 0) {
        LOG_INFO("Rasterization took %u MS.\n", raster_end - raster_start);
    }
#endif

    return 0;
}

uint8_t canvas_destroy()
{
    if (!is_initialized) {
        return 1;
    }

    list_destroy(&line_list);
    list_destroy(&circ_list);
    memset((void *)&canvas, 0, sizeof(struct canvas));
    return 0;
}

uint8_t canvas_handle_mouse(uint32_t state, int32_t x, int32_t y)
{
    if (!is_initialized) {
        return 1;
    }

    // Se mouse não estiver em cima do canvas, não há nada a fazer.
    if (rect_contains_point(&canvas.placement, x, y) != 0) {
        return 1;
    }

    if (canvas.state == CANVAS_STATE_NONE) {
        return 0;
    }

    // Retirar o offset do canvas dos pontos.
    x -= canvas.placement.x;
    y -= canvas.placement.y;

    uint8_t state_finished = 1;
    switch (canvas.state) {
        case CANVAS_STATE_DDA_LINE:
            state_finished = canvas_line_state_machine(state, x, y);
            break;
        case CANVAS_STATE_BRESENHAM_LINE:
            state_finished = canvas_line_state_machine(state, x, y);
            break;
        case CANVAS_STATE_BRESENHAM_CIRCUMFERENCE:
            state_finished = canvas_circumference_state_machine(state, x, y);
            break;
        case CANVAS_STATE_COHEN_SUTHERLAND:
        case CANVAS_STATE_LIANG_BARSKY:
            state_finished = canvas_line_clipping_state_machine(state, x, y);
            break;
        default:
            __builtin_unreachable();
    }

    if (state_finished == 0) {
        canvas_quit_state();
    }

    return 0;
}

uint8_t canvas_set_bg_color(const struct color *c)
{
    if (!is_initialized || !c) {
        return 1;
    }

    memcpy((void *)&canvas.background_color,
           (const void *)c,
           sizeof(struct color));

    return 0;
}

uint8_t canvas_set_fg_color(const struct color *c)
{
    if (!is_initialized || !c) {
        return 1;
    }

    memcpy((void *)&canvas.foreground_color,
           (const void *)c,
           sizeof(struct color));

    return 0;
}

const struct rect *canvas_get_placement()
{
    if (!is_initialized) {
        return NULL;
    }

    return &canvas.placement;
}

uint8_t canvas_set_pixel_coord(int32_t x, int32_t y)
{
    if (!is_initialized) {
        return 1;
    }

    if (x < 0 || y < 0 || x >= canvas.placement.w || y >= canvas.placement.h) {
        return 1;
    }

    SDL_RenderDrawPoint(window_get_renderer(),
                        canvas.placement.x + x,
                        canvas.placement.y + y);
    return 0;
}

void canvas_quit_state()
{
    if (!is_initialized) {
        return;
    }

    canvas.state = CANVAS_STATE_NONE;
    // Caso os eixos estejam ligados, os deixe.
    canvas.state_options &= CANVAS_NEEDS_AXIS_BIT;
    memset((void *)&canvas.tools, 0, sizeof(canvas.tools));
    toolbox_set_current_mode_text(
        font_get_text_with_id(NOTHING_SELECTED_TEXT_ID));
}

static uint8_t canvas_line_state_machine(uint32_t state, int32_t x, int32_t y)
{
    switch (canvas.tools.line_tool.state) {
        case LINE_TOOL_STATE_WAITING_POINT_1:
            if ((state & SDL_BUTTON_LMASK) != 0) {
                canvas.tools.line_tool.line.start_x   = x;
                canvas.tools.line_tool.line.start_y   = y;
                canvas.tools.line_tool.debounce_timer = SDL_GetTicks();

                canvas.tools.line_tool.state = LINE_TOOL_STATE_DEBOUNCE;
            }
            return 1;
        case LINE_TOOL_STATE_DEBOUNCE:
            canvas.tools.line_tool.line.end_x = x;
            canvas.tools.line_tool.line.end_y = y;
            if (SDL_GetTicks()
                > canvas.tools.line_tool.debounce_timer + DEBOUNCE_MS) {
                canvas.tools.line_tool.state = LINE_TOOL_STATE_WAITING_POINT_2;
            }
            return 1;
        case LINE_TOOL_STATE_WAITING_POINT_2:
            canvas.tools.line_tool.line.end_x = x;
            canvas.tools.line_tool.line.end_y = y;
            if ((state & SDL_BUTTON_LMASK) != 0) {
                struct line line;

                // Detalhe, garantimos aqui, que em ambos eixos, start
                // sempre seja menor que end. O algoritmo de Liang Barsky
                // usa essa ordem.

                // Para isso podemos calcular a distância entre os pontos e a
                // origem e colocar o que está mais perto em start.
                const double start_dist =
                    hypot(canvas.tools.line_tool.line.start_x,
                          canvas.tools.line_tool.line.start_y);
                const double end_dist =
                    hypot(canvas.tools.line_tool.line.end_x,
                          canvas.tools.line_tool.line.end_y);
                if (start_dist < end_dist) {
                    line.start_x = canvas.tools.line_tool.line.start_x;
                    line.start_y = canvas.tools.line_tool.line.start_y;
                    line.end_x   = canvas.tools.line_tool.line.end_x;
                    line.end_y   = canvas.tools.line_tool.line.end_y;
                } else {
                    line.start_x = canvas.tools.line_tool.line.end_x;
                    line.start_y = canvas.tools.line_tool.line.end_y;
                    line.end_x   = canvas.tools.line_tool.line.start_x;
                    line.end_y   = canvas.tools.line_tool.line.start_y;
                }

                if (canvas.state == CANVAS_STATE_DDA_LINE) {
                    line.type = LINE_DDA_BIT;
                } else if (canvas.state == CANVAS_STATE_BRESENHAM_LINE) {
                    line.type = LINE_BRESENHAM_BIT;
                }
                list_push_back(&line_list, (uint8_t *)&line);

                LOG_INFO("Line: %ix1 %iy1 %ix2 %iy2.\n",
                         canvas.tools.line_tool.line.start_x,
                         canvas.tools.line_tool.line.start_y,
                         x,
                         y);
                return 0;
            }
            return 1;
        default:
            __builtin_unreachable();
    }

    __builtin_unreachable();
}

static uint8_t canvas_circumference_state_machine(uint32_t state,
                                                  int32_t x,
                                                  int32_t y)
{
    if (canvas.tools.circ_tool.state != CIRC_TOOL_STATE_WAITING_CENTER) {
        // Calculamos a distância do mouse até o centro, esse é o raio da
        // circunferência.
        const int32_t delta_x = canvas.tools.circ_tool.circ.center_x - x;
        const int32_t delta_y = canvas.tools.circ_tool.circ.center_y - y;
        canvas.tools.circ_tool.circ.radius = round(hypot(delta_x, delta_y));
    }

    switch (canvas.tools.circ_tool.state) {
        case CIRC_TOOL_STATE_WAITING_CENTER:
            if ((state & SDL_BUTTON_LMASK) != 0) {
                canvas.tools.circ_tool.circ.center_x  = x;
                canvas.tools.circ_tool.circ.center_y  = y;
                canvas.tools.circ_tool.debounce_timer = SDL_GetTicks();

                canvas.tools.circ_tool.state = CIRC_TOOL_STATE_DEBOUNCE;
            }
            return 1;
        case CIRC_TOOL_STATE_DEBOUNCE:
            canvas.tools.circ_tool.last_mouse_x = x;
            canvas.tools.circ_tool.last_mouse_y = y;
            if (SDL_GetTicks()
                > canvas.tools.circ_tool.debounce_timer + DEBOUNCE_MS) {
                canvas.tools.circ_tool.state = CIRC_TOOL_STATE_WAITING_RADIUS;
            }
            return 1;
        case CIRC_TOOL_STATE_WAITING_RADIUS:
            canvas.tools.circ_tool.last_mouse_x = x;
            canvas.tools.circ_tool.last_mouse_y = y;
            if ((state & SDL_BUTTON_LMASK) != 0) {
                struct circumference circumference;
                circumference.center_x = canvas.tools.circ_tool.circ.center_x;
                circumference.center_y = canvas.tools.circ_tool.circ.center_y;
                circumference.radius   = canvas.tools.circ_tool.circ.radius;
                list_push_back(&circ_list, (uint8_t *)&circumference);

                LOG_INFO("Circumference: %ix %iy %ur.\n",
                         circumference.center_x,
                         circumference.center_y,
                         circumference.radius);
                return 0;
            }

            return 1;
        default:
            __builtin_unreachable();
    }

    __builtin_unreachable();
}

static uint8_t canvas_line_clipping_state_machine(uint32_t state,
                                                  int32_t x,
                                                  int32_t y)
{
    static int32_t first_x  = 0;
    static int32_t first_y  = 0;
    static int32_t second_x = 0;
    static int32_t second_y = 0;

    switch (canvas.tools.line_clipping_tool.state) {
        case LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_1:
            if ((state & SDL_BUTTON_LMASK) != 0) {
                first_x = x;
                first_y = y;

                canvas.tools.line_clipping_tool.clip_rect.x = x;
                canvas.tools.line_clipping_tool.clip_rect.y = y;
                canvas.tools.line_clipping_tool.clip_rect.w = 0;
                canvas.tools.line_clipping_tool.clip_rect.h = 0;

                canvas.tools.line_clipping_tool.debounce_timer = SDL_GetTicks();
                canvas.tools.line_clipping_tool.state =
                    LINE_CLIPPING_TOOL_STATE_DEBOUNCE_3;
            }
            return 1;
        case LINE_CLIPPING_TOOL_STATE_DEBOUNCE_3:
            {
                second_x = x;
                second_y = y;

                const int32_t max_x = max(first_x, second_x);
                const int32_t min_x = min(first_x, second_x);
                const int32_t max_y = max(first_y, second_y);
                const int32_t min_y = min(first_y, second_y);

                canvas.tools.line_clipping_tool.clip_rect.x = min_x;
                canvas.tools.line_clipping_tool.clip_rect.y = min_y;
                canvas.tools.line_clipping_tool.clip_rect.w = max_x - min_x;
                canvas.tools.line_clipping_tool.clip_rect.h = max_y - min_y;

                if (SDL_GetTicks()
                    > canvas.tools.line_clipping_tool.debounce_timer
                          + DEBOUNCE_MS) {
                    canvas.tools.line_clipping_tool.state =
                        LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_2;
                }
            }
            return 1;
        case LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_2:
            {
                second_x = x;
                second_y = y;

                const int32_t max_x = max(first_x, second_x);
                const int32_t min_x = min(first_x, second_x);
                const int32_t max_y = max(first_y, second_y);
                const int32_t min_y = min(first_y, second_y);

                canvas.tools.line_clipping_tool.clip_rect.x = min_x;
                canvas.tools.line_clipping_tool.clip_rect.y = min_y;
                canvas.tools.line_clipping_tool.clip_rect.w = max_x - min_x;
                canvas.tools.line_clipping_tool.clip_rect.h = max_y - min_y;

                if ((state & SDL_BUTTON_LMASK) != 0) {
                    if (canvas.state == CANVAS_STATE_COHEN_SUTHERLAND) {
                        line_clipping_cohen_sutherland(
                            &canvas.tools.line_clipping_tool.clip_rect,
                            &line_list);
                    } else {
                        line_clipping_liang_barsky(
                            &canvas.tools.line_clipping_tool.clip_rect,
                            &line_list);
                    }
                    return 0;
                }
            }
            return 1;
        default:
            __builtin_unreachable();
    }
    __builtin_unreachable();
}

// -----------------------------------------------Abaixo, somente callbacks de
// botões-----------------------------------------------

void canvas_clear()
{
    if (!is_initialized) {
        return;
    }

    // Limpa as listas de formas.
    while (list_pop_back(&line_list, NULL) == 0)
        ;
    while (list_pop_back(&circ_list, NULL) == 0)
        ;
}

void canvas_set_draw_dda_line_state()
{
    if (!is_initialized) {
        return;
    }

    // Se o estado já for esse, nós o desabilitamos.
    if (canvas.state == CANVAS_STATE_DDA_LINE) {
        canvas_quit_state();
        return;
    }

    canvas.state                 = CANVAS_STATE_DDA_LINE;
    canvas.tools.line_tool.state = LINE_TOOL_STATE_WAITING_POINT_1;
}

void canvas_set_draw_bresenham_line_state()
{
    if (!is_initialized) {
        return;
    }

    // Se o estado já for esse, nós o desabilitamos.
    if (canvas.state == CANVAS_STATE_BRESENHAM_LINE) {
        canvas_quit_state();
        return;
    }

    canvas.state                 = CANVAS_STATE_BRESENHAM_LINE;
    canvas.tools.line_tool.state = LINE_TOOL_STATE_WAITING_POINT_1;
}

void canvas_set_draw_bresenham_circumference_state()
{
    if (!is_initialized) {
        return;
    }

    // Se o estado já for esse, nós o desabilitamos.
    if (canvas.state == CANVAS_STATE_BRESENHAM_CIRCUMFERENCE) {
        canvas_quit_state();
        return;
    }

    canvas.state                 = CANVAS_STATE_BRESENHAM_CIRCUMFERENCE;
    canvas.tools.circ_tool.state = CIRC_TOOL_STATE_WAITING_CENTER;
}

void canvas_translate_positive_x()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_translation(&line_list,
                                 &circ_list,
                                 settings_get_offset_x(),
                                 0);
}

void canvas_translate_negative_x()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_translation(&line_list,
                                 &circ_list,
                                 -settings_get_offset_x(),
                                 0);
}

void canvas_translate_positive_y()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_translation(&line_list,
                                 &circ_list,
                                 0,
                                 settings_get_offset_y());
}

void canvas_translate_negative_y()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_translation(&line_list,
                                 &circ_list,
                                 0,
                                 -settings_get_offset_y());
}

void canvas_rotate()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_rotation(&line_list, &circ_list);
}

void canvas_scale()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_scaling(&line_list, &circ_list);
}

void canvas_reflect_x()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_reflection(&line_list, &circ_list, X_AXIS_REFLECTION_BIT);
}

void canvas_reflect_y()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_reflection(&line_list, &circ_list, Y_AXIS_REFLECTION_BIT);
}

void canvas_reflect_xy()
{
    if (!is_initialized) {
        return;
    }

    transforms_apply_reflection(&line_list,
                                &circ_list,
                                X_AXIS_REFLECTION_BIT | Y_AXIS_REFLECTION_BIT);
}

void canvas_set_cohen_sutherland_state()
{
    if (!is_initialized) {
        return;
    }

    // Se o estado já for esse, nós o desabilitamos.
    if (canvas.state == CANVAS_STATE_COHEN_SUTHERLAND) {
        canvas_quit_state();
        return;
    }

    canvas.state = CANVAS_STATE_COHEN_SUTHERLAND;
    canvas.tools.line_clipping_tool.state =
        LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_1;
    canvas.state_options |= CANVAS_NEEDS_RECT_BIT;
}

void canvas_set_liang_barsky_state()
{
    if (!is_initialized) {
        return;
    }

    // Se o estado já for esse, nós o desabilitamos.
    if (canvas.state == CANVAS_STATE_LIANG_BARSKY) {
        canvas_quit_state();
        return;
    }

    canvas.state = CANVAS_STATE_LIANG_BARSKY;
    canvas.tools.line_clipping_tool.state =
        LINE_CLIPPING_TOOL_STATE_WAITING_AREA_POINT_1;
    canvas.state_options |= CANVAS_NEEDS_RECT_BIT;
}

void canvas_toggle_axis()
{
    if (!is_initialized) {
        return;
    }

    if ((canvas.state_options & CANVAS_NEEDS_AXIS_BIT) != 0) {
        canvas.state_options &= ~CANVAS_NEEDS_AXIS_BIT;
    } else {
        canvas.state_options |= CANVAS_NEEDS_AXIS_BIT;
    }
}
