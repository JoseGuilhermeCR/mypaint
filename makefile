#MIT License
#
#Copyright (c) 2021 José Rodrigues (joseguilhermebh@hotmail.com)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
#

# Lista de arquivos que serão compilados.
SRC = $(wildcard src/*.c c-list/*.c)

# Arquivos .o e .d correspondentes.
OBJ = $(SRC:.c=.o)
DEP = $(OBJ:.o=.d)

# Binário padrão que vem com SDL2.
SDL2_CONFIG ?= sdl2-config

# Compilador, linker e assembler.
CC := $(PREFIX)gcc
LD := $(PREFIX)ld
AS := $(PREFIX)as

# Agora defina o prefixo padrão.
PREFIX ?= x86_64_linux-

# Diretório da build.
OUTPUT_DIR = $(PREFIX:-=)

# Diretório onde ficam as fontes.
FONT_DIR = font

# Saída da compilação.
OUTPUT = $(OUTPUT_DIR)/cg

# Flags do compilador.
CC_FLAGS += -Wall -Wextra --std=gnu11 -MMD -O3
# Flags do pré-processador.
PP_FLAGS += -Iinclude/ -Ic-list/ $(shell $(SDL2_CONFIG) --cflags)
# Flags do linker.
LD_FLAGS += $(shell $(SDL2_CONFIG) --libs) -lSDL2_ttf -lm

%.o: %.c
	$(CC) $(CC_FLAGS) $(PP_FLAGS) -c $< -o $@

$(OUTPUT): $(OBJ)
	mkdir -p $(OUTPUT_DIR)
	cp -r $(FONT_DIR) $(OUTPUT_DIR)/$(FONT_DIR)
	$(CC) $^ $(LD_FLAGS) -o $@

-include $(DEP)

.PHONY: cleandep
cleandep:
	rm -f $(OBJ) $(DEP)

.PHONY: clean
clean:
	rm -rf $(OUTPUT_DIR) $(OUTPUT) $(OBJ) $(DEP)

.PHONY: run
run: $(OUTPUT)
	./$(OUTPUT)

.PHONY: debug
debug: $(OUTPUT)
	gdb -ex "run" ./$(OUTPUT)

